FRONT=front
DOCKER_COMPOSE=docker-compose.yml
DOCKER_COMPOSE_PROD=docker-compose.prod.yml

dcr := docker-compose exec -T php

start-api:
	docker-compose up -d && ${dcr} /bin/sh

stop-api:
	docker-compose stop

start-front:
	cd front && npx rollup -w -c rollup.config.mjs &
	yarn --cwd ${FRONT} run dev --https --port 3000 &
	/opt/google/chrome/chrome --ignore-certificate-errors --unsafely-treat-insecure-origin-as-secure=https://localhost:3000 --user-data-dir=/tmp/nova https://localhost:3000

build-dev:
	cp -r front api/front
	docker-compose -f ${DOCKER_COMPOSE} build --pull
	rm -rf api/front

build:
	cp -r front api/front
	docker-compose -f ${DOCKER_COMPOSE} -f ${DOCKER_COMPOSE_PROD} build --pull
	rm -rf api/front

test-api:
	${dcr} vendor/bin/phpunit

test-front:
	yarn --cwd ${FRONT} run test

lint:
	${dcr} bin/console lint:container
	${dcr} vendor/bin/phpstan analyse --level 5 src

migration:
	${dcr} bin/console make:migration

migrate:
	${dcr} bin/console doctrine:migrations:migrate -q

rollback:
	${dcr} bin/console doctrine:migration:migrate prev

api-schema:
	${dcr} bin/console api:openapi:export --yaml --output=openapi.yaml
	cd front/src/types && npx openapi-typescript ../../../api/openapi.yaml --output schema.ts

backup-db:
	docker-compose exec database pg_dump -c -U api-platform api > api/db_dumps/dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
