/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function Lid({width, height}) {
	return (
		<svg height={height} width={width} fill="currentColor" xmlns="http://www.w3.org/2000/svg"
		     viewBox="0 0 512 512" x="0px" y="0px">
			<path d="M496,392.052H16a6,6,0,0,0,0,12H496a6,6,0,0,0,0-12Z"/>
			<path d="M259.117,142.7a40.817,40.817,0,0,0,40.313-34.753H218.8A40.818,40.818,0,0,0,259.117,142.7Z"/>
			<path
				d="M32.364,360.416H479.637a6,6,0,0,0,0-12H463.163C461.847,243.8,369.43,159.065,256,159.065S50.152,243.8,48.836,348.416H32.364a6,6,0,1,0,0,12Zm65.3-104.669c12.6-22.975,33.554-41.209,59-51.346a6,6,0,0,1,4.44,11.149c-22.866,9.107-41.661,25.431-52.923,45.968a6,6,0,0,1-10.521-5.771Z"/>
		</svg>
	)
}
