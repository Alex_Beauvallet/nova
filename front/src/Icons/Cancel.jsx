/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function Cancel({width, height}) {
	return (
		<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
		     x="0px" y="0px" width={width} height={height}
		     viewBox="0 0 477.867 477.867" xmlSpace="preserve" fill="currentColor">
			<g>
				<g>
					<path d="M238.933,0C106.974,0,0,106.974,0,238.933s106.974,238.933,238.933,238.933s238.933-106.974,238.933-238.933
			C477.726,107.033,370.834,0.141,238.933,0z M106.224,82.738c37.059-31.451,84.104-48.681,132.709-48.604
			c48.406-0.015,95.237,17.197,132.113,48.555L82.62,371.115C9.505,284.963,20.073,155.853,106.224,82.738z M371.643,395.129
			c-37.059,31.451-84.104,48.681-132.709,48.604c-48.406,0.015-95.237-17.197-132.113-48.555l288.427-288.427
			C468.362,192.903,457.794,322.014,371.643,395.129z"/>
				</g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
			<g>
			</g>
		</svg>
	)
}
