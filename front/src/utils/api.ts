/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {paths} from "../types/schema";

type Get<T extends any, K extends any[], D = never> = K extends []
    ? T
    : K extends [infer A, ...infer B]
        ? A extends keyof T
            ? Get<T[A], B>
            : D
        : D

type HTTPSuccess = 200 | 201 | 204;

type PickDefined<Object> = Pick<Object, { [Key in keyof Object]: Object[Key] extends undefined ? never : Key }[keyof Object]>

type FetchOptions<Method, Query, Params, Json> =
    RequestInit
    & { method: Method }
    & PickDefined<{ query: Query, params: Params, json: Json }>

type ApiResponse<Path, Method, Type = "application/json"> = Get<paths,
    [Path, Method, "responses", HTTPSuccess, "content", Type]>;

type EndpointParameter<Endpoint, ParameterType extends string> = Endpoint extends { parameters: Record<ParameterType, object> } ? Endpoint["parameters"][ParameterType] : undefined
type QueryParameter<Endpoint> = EndpointParameter<Endpoint, "query">
type PathParameter<Endpoint> = EndpointParameter<Endpoint, "path">

type RequestBody<Endpoint, BodyType extends string = "application/json"> = Endpoint extends { requestBody: { content: Record<BodyType, object> } } ? Endpoint["requestBody"]["content"][BodyType] : undefined

/**
 * Custom fetch function adapted to the API
 * @param baseUrl Base URL of the API
 * @param endpoint Route
 * @param options FetchOptions (method,
 * @param authToken
 */
export async function fetchAPI<Path extends keyof paths, Method extends keyof paths[Path]>(baseUrl: string, endpoint: Path, options: FetchOptions<Method, QueryParameter<paths[Path][Method]>, PathParameter<paths[Path][Method]>, RequestBody<paths[Path][Method]>>, authToken?: String): Promise<ApiResponse<Path, Method>> {
    const o = {
        headers: authToken ? {
            'Authorization': 'Bearer ' + authToken
        } : {},
        ...options,
    } as RequestInit & {
        json?: object;
        headers: Record<string, string>;
        query?: Record<string, any>;
        params?: Record<string, any>;
    };
    const query = o.query;
    const params = o.params;
    let url = baseUrl + endpoint;
    o.headers["Accept"] = "application/json";
    if (o.json) {
        o.body = JSON.stringify(o.json);
        o.headers["Content-Type"] = "application/json";
    }
    if (query) {
        const params = new URLSearchParams();
        Object.keys(query).forEach((k: string) => {
            if (query[k] !== undefined) {
                params.set(k, query[k]);
            }
        });
        url += `?${params.toString()}`;
    }
    if (params) {
        Object.keys(params).forEach(
            (k) => (url = url.replace(`{${k}}`, params[k]))
        );
    }
    return fetch(url, o).then((r) => {
        if (r.status === 204) {
            return null;
        }
        if (r.status >= 200 && r.status < 300) {
            return r.json();
        }
        throw r;
    });
}