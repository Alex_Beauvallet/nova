/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useEffect, useState} from 'react'
import Observations from "../../components/Visit/Observations/Observations"
import {getPredefinedObservations} from "../../services/observations"
import {useFetchAPI} from "../../services/authentication"

export default function ObservationPage({token, event_id}) {

	/**
	 * State used to store predefined observations
	 */
	const [predefinedObservations, setPredefinedObservations] = useState(null)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Effect used to fetch predefined observations from the API and to add default option
	 */
	useEffect(() => {
		getPredefinedObservations(API, token, "client").then(v => setPredefinedObservations([
			{
				id: 0,
				value: "Choix d'une observation prédéfinie..."
			},
			...v
		]))
	}, [])

	return (
		<div className={"flex flex-col items-center justify-center"}>
			<Observations event_id={event_id} type={"client"}
			              predefinedObservations={predefinedObservations}
			              label={"Observations diverses"}/>
			<Observations event_id={event_id} type={"intern"}
			              predefinedObservations={predefinedObservations}
			              label={"Observations internes"}/>
		</div>
	)
}
