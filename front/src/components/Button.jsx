/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export const buttonColorCorrespondances = {
	"primary": ["bg-indigo-600", "hover:bg-indigo-700", "focus:ring-indigo-500", "focus:ring-offset-indigo-200"],
	"secondary": ["bg-gray-600", "hover:bg-gray-700", "focus:ring-gray-500", "focus:ring-offset-gray-200"]
}


export default function Button({children, type, onClick, disabled}) {
	return (
		<button type="button" onClick={onClick} disabled={disabled}
		        className={`py-2 px-4 ${buttonColorCorrespondances[type].join(' ')} text-white transition ease-in duration-200 text-center text- font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2 rounded-lg m-2 max-w-max disabled:opacity-50 disabled:cursor-not-allowed`}>
			{children}
		</button>
	)
}
