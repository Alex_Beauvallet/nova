/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

/**
 *
 * @param error {Error}
 * @returns {JSX.Element}
 * @constructor
 */
export default function ErrorPopup({error}) {
	return <div className={"flex justify-center items-center min-h-screen"}>
		<div className="bg-gray-50 lg:flex lg:flex-col lg:items-center lg:justify-center lg:gap-2">
			<div
				className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between lg:gap-10">
				<h2 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl flex flex-col gap-6">
					<span className="block">Une erreur a eu lieu sur l'application Nova.</span>
					<span className="block text-indigo-600">L'administrateur a été averti.</span>
				</h2>
				<div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
					<div className="inline-flex rounded-md shadow">
						<button
							className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700"
							onClick={() => window.location = `/dashboard`}
						>
							Retour
						</button>
					</div>
				</div>
			</div>
			<div className={"text-center font-bold"}>
				{error ? error.message : null}
			</div>
			<div className={"text-center max-w-7xl bg-gray-300 mt-2 p-4 rounded-lg mx-2 font- leading-relaxed"}>
				{error ? error.stack : null}
			</div>

		</div>
	</div>
}
