/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function NextVisitChoiceItem({visitType, date, duration}) {
	return (
		<>
			<div className="flex items-center">
				<div className="text-sm">
					<p className="font-medium text-gray-800">
						{visitType}
					</p>
					<div className="text-gray-500">
						<p className="sm:inline">Date limite : {date}</p>
					</div>
				</div>
			</div>
			<div className="mt-2 flex justify-center items-center text-sm sm:mt-0 sm:ml-4 sm:text-right">
				<div className="font-medium text-gray-500">{duration}</div>
			</div>
		</>
	)
}
