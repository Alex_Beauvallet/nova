/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback, useEffect, useState} from "react"
import RadioSlack from "../../RadioSlack/RadioSlack"
import NextVisitChoiceItem from "./NextVisitChoiceItem"
import TextBadge from "../../TextBadge"
import {
	getConnectedStationsToControl,
	getNewWorksiteStatus,
	getNextVisitDate,
	getNextVisitMetaData,
	getNextVisitTypes,
	getVisitTypeDuration,
	VisitStepTypesCharacteristics
} from "../../../services/nextVisit"
import {useFetchAPI} from "../../../services/authentication"
import CharacteristicDurationsTable from "./CharacteristicDurationsTable"
import {useDispatch, useSelector} from "react-redux"
import {initializeNextVisit, updateChoice, updateDuration} from "./nextVisitSlice"
import {findMinutes, fromMinutes} from "pomeranian-durations"


export default function NextVisitChoice({token, worksite, event_id, setIsButtonAvailable}) {
	/**
	 * Local state of all available next visit options
	 */
	const [nextVisitOptions, setNextVisitOptions] = useState([])

	/**
	 * Local state of characteristic durations
	 */
	const [characteristicDurations, setCharacteristicDurations] = useState([])

	/**
	 * New adjustments from redux store
	 */
	const newAdjustments = useSelector(/**
	 * @param state {{nextVisit: Array}}
	 */
	state => state.nextVisit[event_id] ? state.nextVisit[event_id].adjustments : {})

	/**
	 * Next visit type choice from redux store
	 */
	const choice = useSelector(/**
	 * @param state {{nextVisit: Array}}
	 */
	state => state.nextVisit[event_id] ? state.nextVisit[event_id].choice : null)

	/**
	 * Eliminations from redux store
	 * @type {unknown}
	 */
	const eliminations = useSelector(/**
	 * @param state {{eliminations: Array}}
	 */
	state => state.eliminations[event_id] ?? null)

	/**
	 * SSOL stations from redux store
	 * @type {unknown}
	 */
	const SSOLStations = useSelector(/**
	 * @param state {{stations: Array}}
	 */
	state => state.stations[event_id]?.["ssol"] ?? null)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Effect used to load next visit types from the API
	 */
	useEffect(() => {
		getNextVisitTypes(API, token, getNewWorksiteStatus(eliminations, SSOLStations, worksite.status.short_name), false, worksite.id).then(setNextVisitOptions)
		VisitStepTypesCharacteristics(API, token, worksite.id).then(setCharacteristicDurations)
		// noinspection JSCheckFunctionSignatures
		dispatch(initializeNextVisit({
			event_id: event_id
		}))
		setIsButtonAvailable(!!choice)
	}, [API, token, worksite])

	/**
	 * Effect used to update next visit duration when adjustments change
	 */
	useEffect(() => {
		if (choice) {
			// noinspection JSCheckFunctionSignatures
			dispatch(updateDuration({
				event_id
			}))
		}
	}, [newAdjustments])

	/**
	 * Callback which hydrates NextVisitChoice options from nextVisitOptions
	 * @type {Function}
	 */
	const hydrateOptions = useCallback(() => {
		return nextVisitOptions.map(option => {
			if (option.code === "SSOLconnec" && option.steps[0].isDurationUnit) {
				const unitSSOL = findMinutes(option.steps[0].duration)
				if (unitSSOL > 0) {
					option.steps[0].isDurationUnit = false
					option.steps[0].duration = fromMinutes(getConnectedStationsToControl(worksite, SSOLStations).length * unitSSOL)
				}
			}
			const {color, formattedDate, formattedDuration} = getNextVisitMetaData(option, worksite, newAdjustments)
			return {
				id: option.id,
				borderColor: color,
				value: <NextVisitChoiceItem visitType={option.long_name} date={formattedDate}
				                            duration={<TextBadge small={true}
				                                                 color={color}>{formattedDuration}</TextBadge>}/>,
			}
		})
	}, [nextVisitOptions, newAdjustments, worksite, SSOLStations])

	/**
	 * Callback used to select next visit type
	 * @type {Function}
	 */
	const selectOptionCallback = useCallback(value => {
		const option = nextVisitOptions.filter(o => o.id === value)?.[0]
		if (option) {
			// noinspection JSCheckFunctionSignatures
			dispatch(updateChoice({
				event_id,
				value: {
					...option,
					duration: getVisitTypeDuration(option, newAdjustments),
					date: getNextVisitDate(option, worksite).toISOString()
				}
			}))
			setIsButtonAvailable(true)
		}
	}, [nextVisitOptions, newAdjustments])

	return (
		<>
			<RadioSlack options={hydrateOptions(nextVisitOptions)} select={selectOptionCallback}
			            selected={choice?.id ?? null}/>
			{Object.keys(newAdjustments).length > 0 && characteristicDurations.length > 0 &&
				<CharacteristicDurationsTable durations={characteristicDurations} newAdjustments={newAdjustments}
				                              event_id={event_id}/>}
		</>
	)
}
