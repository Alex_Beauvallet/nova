/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {createSlice} from "@reduxjs/toolkit"
import {getVisitTypeDuration} from "../../../services/nextVisit"


export const nextVisitSlice = createSlice({
	name: "nextVisit",
	initialState: {},
	reducers: {
		initializeNextVisit: (state, {payload: {event_id, type}}) => {
			if (!state?.[event_id]) {
				return {
					...state,
					[event_id]: {
						...state?.[event_id],
						adjustments: {
							...state?.[event_id]?.adjustments,
							ssol_all: 0,
							sb: 0,
							cb: 0
						},
						choice: null
					}
				}
			}
		},
		updateAdjustments: (state, {payload: {event_id, type, value}}) => {
			state[event_id].adjustments[type] = value
		},
		updateChoice: (state, {payload: {event_id, value}}) => {
			state[event_id].choice = value
		},
		updateDuration: (state, {payload: {event_id}}) => {
			state[event_id].choice = {
				...state[event_id].choice,
				duration: getVisitTypeDuration(state[event_id].choice, state[event_id].adjustments)
			}
		},
		removeNextVisitData: (state, action) => {
			delete state[action.payload.event_id]
		},
	}
})

export const {
	initializeNextVisit,
	updateAdjustments,
	updateChoice,
	updateDuration,
	removeNextVisitData
} = nextVisitSlice.actions

export default nextVisitSlice.reducer
