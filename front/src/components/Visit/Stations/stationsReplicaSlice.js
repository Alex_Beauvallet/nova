/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {createSlice} from "@reduxjs/toolkit"
import {initializeReplicaStations} from "../../../services/stations"


export const stationsReplicaSlice = createSlice({
	name: "stationsReplica",
	initialState: {},
	reducers: {
		initializeReplicaTable: (state, {payload: {event_id, type, stations}}) => {
			if (!state[event_id]?.[type]) {
				return initializeReplicaStations(stations, type, event_id, state)
			}
		},
		addReplica: (state, {payload: {event_id, type, value, indexName}}) => {
			state[event_id][type] = {
				...state[event_id][type],
				[value.station]: [
					...(state[event_id][type][value.station] ?? []),
					indexName
				]
			}
		},
		removeReplica: (state, {payload: {event_id, type, station, indexName}}) => {
			const replicas = state[event_id][type][station]
			state[event_id][type][station] = replicas.filter(v => v !== indexName)
		},
		resetReplicaTable: (state, {payload: {event_id, type, stations}}) => {
			return initializeReplicaStations(stations, type, event_id, state)
		},
		removeStationsReplicaData: (state, action) => {
			delete state[action.payload.event_id]
		},
	}
})

export const {
	addReplica,
	removeReplica,
	initializeReplicaTable,
	resetReplicaTable,
	removeStationsReplicaData
} = stationsReplicaSlice.actions

export default stationsReplicaSlice.reducer
