/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback, useEffect, useRef} from "react"
import Td from "../../../Table/Td"
import Text from "../../../Table/Text"
import Select from "../../../Select"
import Checkbox from "../../../Checkbox"
import StationLineContextMenu from "./StationLineContextMenu"
import IconButton from "../../../IconButton"
import Tick from "../../../../Icons/Tick"
import {getIndexNameFromStationsReplicaItem, getStationID} from "../../../../services/stations"
import {useDispatch, useSelector} from "react-redux"
import {installStation} from "../stationsSlice"
import {addReplica, removeReplica} from "../stationsReplicaSlice"

function StationLine({
	                     labels,
	                     worksite,
	                     station,
	                     hiddenStations,
	                     setisLastAddedLineStationNumberChosen,
	                     percentages,
	                     manuallyAdded,
	                     updateStationLine,
	                     deleteStationLine,
	                     setNewStationNumber,
	                     setPopupsOnClick,
	                     setIsPopupVisible,
	                     getStatus,
	                     index,
	                     event_id,
	                     type

                     }) {
	/**
	 * Indicates if all inputs on the line should be disabled
	 * @type {*|boolean}
	 */
	const disabledInput = station.isRecharged || station.isReplaced || station.isRemoved || station.isNotAccessible || station.isNew

	/**
	 * Reference to station number select element, used to get the selected value when the validation button is clicked
	 * @type {React.MutableRefObject<HTMLSelectElement>}
	 */
	const stationNumberSelect = useRef()

	/**
	 * If no station number is selected (ie when a station line is added), set the corresponding state to false to prevent user from adding other lines
	 */
	useEffect(() => {
		if (!station.stationNumber) {
			setisLastAddedLineStationNumberChosen(false)
		}
	}, [])

	/**
	 * StationsReplica store for a specific station.
	 * Contains all replica stations of this station
	 * @type {Object[]}
	 */
	const stationsReplica = useSelector(/**
	 * @param state {{stationsReplica: Array}}
	 */
	state => state.stationsReplica[event_id]?.[type]?.[station.station])

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Callback used to update a station line
	 * If a station is duplicated, adds a replica and install the new station
	 * @type {Function}
	 */
	const updateStationLineCallback = useCallback(payload => {
		updateStationLine(payload)
		if (payload.value.isDuplicated && payload.unitAction) {
			const indexName = getIndexNameFromStationsReplicaItem(stationsReplica)
			console.log(indexName)
			// noinspection JSCheckFunctionSignatures
			dispatch(addReplica({
				...payload,
				indexName: indexName
			}))
			dispatch(installStation({
				event_id: payload.event_id,
				type: payload.type,
				number: payload.value.stationNumber,
				masterStation: payload.value.station,
				indexName: indexName
			}))
		}
	}, [updateStationLine, stationsReplica])

	/**
	 * Callback used to delete a station replica
	 * @type {Function}
	 */
	const deleteStationLineCallback = useCallback(payload => {
		deleteStationLine(payload)
		if (payload.station.masterStation) {
			// noinspection JSCheckFunctionSignatures
			dispatch(removeReplica({
				event_id: payload.event_id,
				type: payload.type,
				station: payload.station.masterStation,
				indexName: payload.station.indexName
			}))
		}
	}, [deleteStationLine])

	return (
		<tr>
			<Td label={labels[0]}>
				{station.stationNumber ?
					<Text>{`${station.stationNumber}${station.indexName ? ` ${station.indexName}` : ''}`}</Text> :
					<div className={"flex justify-center items-center"}>
						<Select name={"actCons"} ref={stationNumberSelect} className={"w-16"} options={hiddenStations}
						/>
						<IconButton icon={<Tick height={15}/>}
						            className={"text-white p-2 rounded shadow bg-green-500 ml-2"} onClick={() => {
							const parts = stationNumberSelect.current.value.split(",")
							updateStationLine({
								event_id: event_id,
								type: type,
								index: index,
								value: {
									...station,
									stationNumber: parseInt(parts[0]),
									station: `/stations/${getStationID(worksite, type, parts[0], parts[1])}`,
									indexName: parts[1] ?? null
								}
							})
							setisLastAddedLineStationNumberChosen(true)
						}
						}/>
					</div>

				}
			</Td>
			<Td label={labels[1]}>
				<Checkbox value={station.isConnected} disabled={disabledInput} onChange={e => {
					const checked = e.target.checked
					updateStationLine({
						event_id: event_id,
						type: type,
						index: index,
						value: {
							...station,
							isConnected: checked,
							consumption: !checked ? 0 : station.consumption
						}
					})
				}}/>
			</Td>
			<Td label={labels[2]}>
				<Text>{station.oldConsumption} %</Text>
			</Td>
			<Td label={labels[3]}>
				<Select name={"consumption"} options={percentages} value={station.consumption} disabled={disabledInput}
				        onChange={e => {
					        const consumption = parseInt(e.target.value)
					        updateStationLine({
						        event_id: event_id,
						        type: type,
						        index: index,
						        value: {
							        ...station,
							        isConnected: consumption > 0,
							        consumption: consumption
						        }
					        })
				        }}/>
			</Td>
			<Td label={labels[4]}>
				<Checkbox value={station.isInActivity} disabled={disabledInput} onChange={e => {
					updateStationLine({
						event_id: event_id,
						type: type,
						index: index,
						value: {
							...station,
							isInActivity: e.target.checked
						}
					})
				}}/>
			</Td>
			<Td label={labels[5]}>
				{getStatus(station)}
			</Td>
			<Td label={labels[6]} defaultLabel={"Action"}>
				<StationLineContextMenu updateStationLine={updateStationLineCallback} station={station} index={index}
				                        manuallyAdded={manuallyAdded} deleteStationLine={deleteStationLineCallback}
				                        setNewStationNumber={setNewStationNumber} event_id={event_id} type={type}
				                        setisLastAddedLineStationNumberChosen={setisLastAddedLineStationNumberChosen}
				                        setIsPopupVisible={setIsPopupVisible}
				                        setPopupsOnClick={setPopupsOnClick}
				                        stationsReplica={stationsReplica}/>
			</Td>
		</tr>
	)
}

export const StationLineMemo = React.memo(StationLine)
