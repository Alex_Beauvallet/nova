/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {createSlice} from "@reduxjs/toolkit"
import {getCommonInitialState, setGlobalStationState} from "../../../services/stations"


export const stationsSlice = createSlice({
	name: "stations",
	initialState: {},
	reducers: {
		addStationLine: (state, action) => {
			state[action.payload.event_id][action.payload.type].push({
				stationNumber: action.payload.number ?? null,
				isConnected: false,
				consumption: 0,
				oldConsumption: 0,
				isInActivity: false,
				manuallyAdded: true,
				...getCommonInitialState(),
				...(action.payload.initialValues ?? {})
			})
		},
		removeStationLine: (state, action) => {
			const stations = state[action.payload.event_id][action.payload.type]
			const oldStation = stations[action.payload.index]
			state[action.payload.event_id][action.payload.type] = stations.filter((value, index) => index !== action.payload.index).map(station => oldStation.isNew && !oldStation.masterStation && station.isNew && station.stationNumber > action.payload.number ? {
				...station,
				stationNumber: station.stationNumber - 1
			} : station)
		},
		updateUnchangedStationNumber: (state, action) => {
			const stations = state[action.payload.event_id][action.payload.type]
			if (stations[stations.length - 1] && !stations[stations.length - 1].stationNumber) {
				stations[stations.length - 1].stationNumber = action.payload.number
			}
		},
		updateStationLine: (state, action) => {
			state[action.payload.event_id][action.payload.type][action.payload.index] = action.payload.value
		},
		installStation: (state, action) => {
			state[action.payload.event_id][action.payload.type].push({
				stationNumber: action.payload.number,
				stationType: `/station_types/${action.payload.type === 'ssol' ? '1' : '2'}`,
				masterStation: action.payload.masterStation,
				indexName: action.payload.indexName,
				isConnected: false,
				consumption: 0,
				oldConsumption: 0,
				isInActivity: false,
				manuallyAdded: true,
				...getCommonInitialState(),
				isNew: true
			})
		},
		resetTable: (state, action) => {
			return setGlobalStationState(state, action.payload.event_id, action.payload.type, action.payload.stations)
		},
		initializeTable: (state, action) => {
			if (!state[action.payload.event_id]?.[action.payload.type]) {
				return setGlobalStationState(state, action.payload.event_id, action.payload.type, action.payload.stations)
			}
		},
		removeStationsData: (state, action) => {
			delete state[action.payload.event_id]
		},
		unmountStations: (state, action) => {
			state[action.payload.event_id][action.payload.type] = state[action.payload.event_id][action.payload.type]?.map(s => (
				{
					...s,
					isRemoved: true
				}
			))
			const newLines = action.payload.stations.map(station => {
				if (station.type.short_name === action.payload.type && !state[action.payload.event_id][action.payload.type].find(s => s.station === `/stations/${station.id}`)) {
					return {
						stationNumber: station.number,
						station: `/stations/${station.id}`,
						stationType: `/station_types/${action.payload.type === 'ssol' ? '1' : '2'}`,
						masterStation: station.masterStation ? `/stations/${station.masterStation.id}` : null,
						indexName: station.indexName,
						isConnected: false,
						consumption: 0,
						oldConsumption: 0,
						isInActivity: false,
						manuallyAdded: true,
						...getCommonInitialState(),
						isRemoved: true
					}
				}
			}).filter(s => s !== undefined)
			state[action.payload.event_id][action.payload.type] = [
				...(state[action.payload.event_id][action.payload.type] ?? []),
				...newLines
			]
		}
	}
})

export const {
	addStationLine,
	removeStationLine,
	updateUnchangedStationNumber,
	updateStationLine,
	installStation,
	resetTable,
	initializeTable,
	removeStationsData,
	unmountStations
} = stationsSlice.actions

export default stationsSlice.reducer
