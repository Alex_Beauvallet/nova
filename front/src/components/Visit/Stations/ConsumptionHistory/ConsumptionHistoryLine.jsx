/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import Th from "../../../Table/Th"
import Td from "../../../Table/Td"

function ConsumptionHistoryLine({number, history, getStatus}) {

	return (
		<tr>
			<Th>
				<div className={"normal-case"}>
					{number}
				</div>
			</Th>
			{history.map((status, index) => status ? <Td key={index}>
				<div className={"flex flex-col justify-center items-center"}>
					<div className={"mb-2"}>
						{getStatus(status)}
					</div>
					<div className={"text-gray-800"}>{status.consumption} %</div>
				</div>
			</Td> : <Td key={index}>-</Td>)}
			<Td/>
		</tr>
	)
}

export const ConsumptionHistoryLineMemo = React.memo(ConsumptionHistoryLine)
