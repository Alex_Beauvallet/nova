/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import EliminationEndBurger from "../Eliminations/EliminationEndBurger"


export default function SpecialStepPage({onReset, title, items, children}) {

	return (
		<div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
			<div className="max-w-4xl mx-auto">
				<div className="bg-white rounded-lg shadow mb-6">
					<div className="border-b border-gray-200 px-4 py-5 sm:px-6">
						<div className="relative flex lg:justify-center items-center">
							<h2 className="text-xl text-indigo-600 font-semibold tracking-wide uppercase">{title}</h2>
							{onReset &&
								<div className={"absolute right-0"}><EliminationEndBurger resetTable={onReset}/></div>}
						</div>
					</div>
					<div className="mx-auto px-4 sm:px-6 lg:px-8">
						<div className="mt-10">
							<dl className="flex flex-col gap-y-8 md:grid md:gap-x-8">
								{items.map((feature) => (
									<div key={feature.name} className={`relative ${feature.className}`}>
										<dt>
											<div
												className="absolute flex items-center justify-center h-12 w-12 rounded-md bg-indigo-500 text-white">
												<feature.icon className="h-6 w-6" aria-hidden="true"/>
											</div>
											<p className="ml-16 text-lg leading-6 font-medium text-gray-900">{feature.name}</p>
										</dt>
										<dd className="mt-2 ml-16 text-base text-gray-500">{feature.description}</dd>
									</div>
								))}
								<div className={"col-span-2 flex justify-center mb-8"}>
									<div>
										{children}
									</div>
								</div>
							</dl>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}
