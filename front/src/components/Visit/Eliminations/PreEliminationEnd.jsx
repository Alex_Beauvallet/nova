/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback, useEffect, useMemo} from "react"
import {useDispatch, useSelector} from "react-redux"
import SpecialStepPage from "../Pages/SpecialStepPage"
import SB from "../../../Icons/SB"
import EliminationEndTable from "./EliminationEndTable"
import {addElimination} from "./EliminationsSlice"
import {getRemovedStations} from "../../../services/stations"


export default function PreEliminationEnd({event_id, visit}) {

	const type = "preEliminationEnd"

	/**
	 * preEliminationEnd object
	 */
	const preEliminationEnd = useSelector(/**
	 * @param state {{eliminations: Array}}
	 */
	state => state.eliminations[event_id]?.[type] ? state.eliminations[event_id][type] : null)

	/**
	 * Dispatch function (redux)
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Array of stations statuses
	 * @type {Object}
	 */
	const stations = useSelector(/**
	 * @param state {{stations: Array}}
	 */
	state => state.stations[event_id] ?? {})

	/**
	 * Callback used to initialize tha table with data (from API or local storage)
	 * @type {Function}
	 */
	const initializeTable = useCallback((stationsOnly = false) => {
		const stationNumber = {
			computedSSOL: visit.worksite.ssolNumber,
			computedSB: stations?.["sb"] ? visit.worksite.sbNumber - getRemovedStations(stations["sb"]) : 0
		}
		if (stationsOnly) {
			dispatch(addElimination({
				event_id: event_id,
				type: type,
				value: {
					...preEliminationEnd,
					...stationNumber
				}
			}))
		} else {
			dispatch(addElimination({
				event_id: event_id,
				type: type,
				value: {
					stateChoice: null,
					...stationNumber
				}
			}))
		}
	}, [visit, event_id, type, stations])

	/**
	 * Callback used to change a value from the elimination table
	 * @type {Function}
	 */
	const onChangeCallback = useCallback((attribute, value) => {
		dispatch(addElimination({
			event_id: event_id,
			type: type,
			value: {
				...preEliminationEnd,
				[attribute]: value
			}
		}))
	}, [preEliminationEnd])

	const preEliminationItems = useMemo(() => {
		if (preEliminationEnd) {
			return [
				{
					name: 'Statut des stations',
					description: <div>
						<div>
							<label className="inline-flex items-center">
								<input type="radio" className="form-radio" name="radio"
								       value="sb_ssol" onChange={v => onChangeCallback("stateChoice", v.target.value)}
								       checked={preEliminationEnd.stateChoice === "sb_ssol"}/>
								<span className="ml-2">Plus de consommation dans les stations SB et SSOL</span>
							</label>
						</div>
						<div>
							<label className="inline-flex items-center">
								<input type="radio" className="form-radio" name="radio"
								       value="sb" onChange={v => onChangeCallback("stateChoice", v.target.value)}
								       checked={preEliminationEnd.stateChoice === "sb"}/>
								<span className="ml-2">Plus de consommation dans les stations SB mais toujours dans une ou plusieurs SSOL</span>
							</label>
						</div>
					</div>,
					icon: SB,
				},
			]
		}
		return []

	}, [preEliminationEnd, visit])

	useEffect(() => {
		if (!preEliminationEnd) {
			initializeTable()
		}
	}, [])

	/**
	 * Effect used to update station number in table when stations changes
	 */
	useEffect(() => {
		initializeTable(true)
	}, [stations])

	return (
		preEliminationEnd &&
		<SpecialStepPage items={preEliminationItems} title={"pré-élimination"} onReset={() => initializeTable(false)}>
			<EliminationEndTable initialSSOL={visit.worksite.ssolNumber}
			                     initialSB={visit.worksite.sbNumber}
			                     computedSB={preEliminationEnd.computedSB}
			                     computedSSOL={preEliminationEnd.computedSSOL}
			                     onChange={onChangeCallback}/>
		</SpecialStepPage>
	)
}
