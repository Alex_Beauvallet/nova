/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useCallback} from "react"
import DatePicker from "../../DatePicker/DatePicker"
import {useDispatch, useSelector} from "react-redux"
import {addElimination} from "./EliminationsSlice"

export default function EliminationBegin({event_id}) {

	const type = "eliminationBegin"

	const eliminationBegin = useSelector(/**
	 * @param state {{eliminations: Array}}
	 */
	state => state.eliminations[event_id]?.[type] ? state.eliminations[event_id][type] : null)

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Callback used to dispatch elimination begin event
	 * @type {Function}
	 */
	const addEliminationBeginCallback = useCallback(v => dispatch(addElimination({
		event_id: event_id,
		type: type,
		value: {
			...eliminationBegin,
			date: v.toISOString()
		}
	})), [event_id, type])

	return (
		<div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:px-8">
			<div className="max-w-2xl mx-auto">
				<div className="bg-white overflow-hidden shadow rounded-lg">
					<div className="border-b border-gray-200 px-4 py-5 sm:px-6">
						<div className="lg:text-center">
							<h2 className="text-xl text-indigo-600 font-semibold tracking-wide uppercase">Début
								d'élimination</h2>
						</div>
					</div>
					<div className="px-4 py-5 sm:p-6">
						<div className={"flex justify-center text-gray-800 pb-3"}>
							<div className={"mr-1"}>Date d'effet :</div>
							<DatePicker selected={eliminationBegin?.date ? new Date(eliminationBegin.date) : new Date()}
							            inputClassName={"basic-calendar-input"} onChange={addEliminationBeginCallback}/>
						</div>
						<div className={"flex text-center justify-center text-gray-800 pt-4 font-bold"}>
							Observation d'une consommation d'appât Recrute
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}
