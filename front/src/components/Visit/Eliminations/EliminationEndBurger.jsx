/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import ContextMenu from "../../ContextMenu/ContextMenu"
import Burger from "../../../Icons/Burger"
import ContextMenuItemWithIcon from "../../ContextMenu/ContextMenuItemWithIcon"
import Previous from "../../../Icons/Previous"

export default function EliminationEndBurger({resetTable}) {
	return (
		<div className={"flex justify-center align-middle"}>
			<ContextMenu openingButton={<div className={"text-white p-2 bg-gray-500 rounded shadow-md"}>
				<Burger width={15}/>
			</div>}
			             buttonClassName={"focus:ring-gray-500"}>
				<ContextMenuItemWithIcon icon={<Previous width={20}/>} onClick={resetTable} className={"text-gray-600"}>
					<div className={"font-semibold text-sm normal-case"}>Réinitialiser la saisie</div>
				</ContextMenuItemWithIcon>
			</ContextMenu>
		</div>
	)
}
