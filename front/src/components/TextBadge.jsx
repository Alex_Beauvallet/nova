/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function TextBadge({color, children, small = false}) {
	switch (color) {
		case "green":
			return <span
				className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-green-800 bg-green-300`}>{children}</span>
		case "yellow":
			return <span
				className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-yellow-800 bg-yellow-200`}>{children}</span>
		case "red":
			return <span
				className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-red-800 bg-red-300`}>{children}</span>
		default:
			return <span
				className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-blue-600 bg-blue-200`}>{children}</span>
	}
}
