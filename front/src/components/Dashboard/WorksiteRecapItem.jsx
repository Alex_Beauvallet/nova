/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function WorksiteRecapItem({label, value, children}) {
	return (
		<div className="flex items-center mb-4 text-blue-500 rounded justify-between">
        <span className="rounded-lg p-2 bg-white">
	        {children}
        </span>
			<div className="flex flex-col w-full ml-2 items-start justify-evenly">
				<p className="text-white text-lg">
					{value}
				</p>
				<p className="text-blue-200 text-sm">
					{label}
				</p>
			</div>
		</div>
	)
}
