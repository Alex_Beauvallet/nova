/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import IconButton from "../IconButton"
import LeftChevron from "../../Icons/LeftChevron"
import RightChevron from "../../Icons/RightChevron"


function processDate(date, operation) {
	const dateObject = new Date(date)
	dateObject.setDate(dateObject.getDate() + operation)
	return dateObject
}

export default function DashBoardTitleDate({date, setDate, datepicker}) {
	return (
		<div className={"flex w-full justify-between"}>
			<IconButton icon={<LeftChevron width={30} height={30}/>} onClick={() => {
				setDate(processDate(date, -1))
			}
			}/>
			<h1 className={"text-2xl text-gray-600"}>Chantiers du {datepicker}</h1>
			<IconButton icon={<RightChevron width={30} height={30}/>} onClick={() => {
				setDate(processDate(date, 1))
			}
			}/>
		</div>
	)
}
