/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function LoadingItem() {
	return (
		<div className="border border-blue-300 shadow rounded-md p-4 mt-6 w-full mx-auto h-52">
			<div className="animate-pulse flex space-x-4 w-full">
				<div className="rounded-full bg-blue-400 h-12 w-12"/>
				<div className="flex-1 space-y-4 py-1">
					<div className="h-4 bg-blue-400 rounded w-3/4"/>
					<div className="space-y-2">
						<div className="h-4 bg-blue-400 rounded"/>
						<div className="h-4 bg-blue-400 rounded w-5/6"/>
					</div>
					<div className="space-y-2">
						<div className="h-4 bg-blue-400 rounded"/>
						<div className="h-4 bg-blue-400 rounded w-5/6"/>
					</div>
					<div className="space-y-2">
						<div className="h-4 bg-blue-400 rounded w-1/3"/>
					</div>
				</div>
			</div>
		</div>
	)
}
