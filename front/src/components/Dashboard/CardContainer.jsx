/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

export default function CardContainer({children, isVisit, hideCondition, className}) {
	return (
		<div
			className={`flex-1 text-center items-center lg:mr-4 lg:ml-4 content-center w-full ${!isVisit ? 'lg:w-auto lg:mb-0 pl-1 mb-4' : 'lg:ml-4 lg:mr-4 mb-6 lg:max-w-xs'} ${hideCondition ? ' lg:hidden' : ''} ${className ? className : ''}`}>
			{children}
		</div>
	)
}
