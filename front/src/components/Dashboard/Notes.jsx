/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import NotesItem from "./NotesItem"
import IconButton from "../IconButton"
import LeftChevron from "../../Icons/LeftChevron"

export default function Notes({setNotesOpen, worksite}) {
	return (
		<div className="bg-white dark:bg-gray-800 shadow overflow-hidden sm:rounded-md">
			<ul className="divide-y divide-gray-200">
				<li>
					<div className={"p-4 flex space-between justify-between"}>
						<IconButton icon={<LeftChevron width={20} height={20}/>} onClick={() => setNotesOpen(false)}/>
						<div className={"text-gray-700 text-xl"}>
							Notes du chantier
						</div>
						<div/>
					</div>
				</li>
				{worksite.notes.map((note, index) =>
					note.is_visible &&
					<li key={`note-${index}`}>
						<NotesItem date={note.created_at} isPonctual={note.is_punctual}>{note.content}</NotesItem>
					</li>
				)}
			</ul>
		</div>
	)
}
