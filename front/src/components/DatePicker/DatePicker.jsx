/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import ReactDatePicker, {registerLocale} from "react-datepicker"
import fr from 'date-fns/locale/fr'
import "react-datepicker/dist/react-datepicker.css"
import "./DatePicker.css"
import {getDay} from "date-fns"

registerLocale('fr', fr)

export default function DatePicker({selected, onChange, inputClassName}) {
	const isWeekday = (date) => {
		const day = getDay(date)
		return day !== 0 && day !== 6
	}
	return (
		<ReactDatePicker selected={selected} onChange={onChange} locale={fr} dateFormat={'dd/MM/yyyy'}
		                 calendarClassName={"calendar"} className={inputClassName ?? "calendar-input"}
		                 wrapperClassName={"calendar-wrapper"}
		                 dayClassName={() => "calendar-day"}
		                 weekDayClassName={() => "calendar-weekday"}
		                 calendarStartDay={1}
		                 filterDate={isWeekday}
		                 disabledKeyboardNavigation={true}
		/>
	)
}
