/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

/**
 * Gets border color of RadioSlackItem
 * @param color
 * @returns {string}
 */
function getBorder(color) {
	switch (color) {
		case "green":
			return "border-green-300 hover:border-green-400"
		case "yellow":
			return "border-yellow-300 hover:border-yellow-400"
		case "red":
			return "border-red-300 hover:border-red-400"
		default:
			return "border-gray-300 hover:border-gray-400"
	}
}

export default function RadioSlackItem({index, active, onClick, borderColor, children}) {
	return (
		<li id="radiogroup-option-0"
		    className="group relative bg-white rounded-lg shadow-sm cursor-pointer focus:outline-none focus:ring-1 focus:ring-offset-2 focus:ring-indigo-500"
		    role={"radio"} aria-checked={active} onClick={onClick} tabIndex={index}>
			<div
				className={`rounded-lg border bg-white px-6 py-4 sm:flex sm:justify-between ${getBorder(borderColor)}`}>
				{children}
			</div>
			<div
				className={`${active ? 'border-indigo-500' : 'border-transparent'} absolute inset-0 rounded-lg border-2 pointer-events-none`}/>
		</li>
	)
}
