/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import {Menu} from "@headlessui/react"

export default function ContextMenuItem({children, onClick, disabled}) {
	return (
		<Menu.Item disabled={disabled}>
			{({active}) => (
				<a
					onClick={onClick}
					className={`cursor-pointer block px-4 py-2 text-sm disabled:opacity-50' ${active ? 'bg-gray-100 text-gray-900' : 'text-gray-700'} ${disabled ? 'cursor-not-allowed opacity-50' : ''}`}
				>
					{children}
				</a>
			)}
		</Menu.Item>
	)
}
