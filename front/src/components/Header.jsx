/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from 'react'

/**
 * Test header component
 *
 * @component
 */
const Header = ({children, icon}) => {
	return (
		<header className="bg-white shadow">
			<div
				className="max-w-7xl mx-auto py-2 px-4 sm:px-6 lg:px-8 flex flex-row items-center justify-between lg:justify-center">
				<div className={"mr-4 lg:hidden"}>
					{icon}
				</div>
				<h1 className="text-3xl font-bold leading-tight text-gray-900 text-center">
					{children}
				</h1>
				<div/>
			</div>
		</header>
	)
}

export default Header
