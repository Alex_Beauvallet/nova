/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {useState} from "react"
import CrossButton from "./CrossButton"

export default function SearchBar({placeholder, onChange, isSearched, onClickCancel}) {
	const [searched, setSearched] = useState("")
	return (
		<div
			className="relative bg-white p-2 rounded border-2 border-white text-gray-400 rounded-m shadow flex items-center">
			<svg className="h-5 w-5 absolute left-0 ml-2" fill="currentColor" viewBox="0 0 20 20">
				<path fillRule="evenodd" fill="currentColor"
				      d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
				      clipRule="evenodd"/>
			</svg>
			<input type="text" placeholder={placeholder} onChange={(e) => {
				setSearched(e.target.value)
				onChange(e)
			}
			}
			       className="ml-8 bg-transparent text-gray-500 w-full focus:outline-none border-0 focus:ring-0 p-0"
			       value={searched}/>
			{isSearched && <CrossButton className={"mr-2"} onClick={(e) => {
				setSearched("")
				onClickCancel(e)
			}}/>}
		</div>
	)
}
