/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"

function getClasses(color) {
	switch (color) {
		case "green":
			return {
				text: "text-green-900",
				bg: "bg-green-200"
			}
		case "yellow":
			return {
				text: "text-yellow-900",
				bg: "bg-yellow-200"
			}
		case "red":
			return {
				text: "text-red-900",
				bg: "bg-red-200"
			}
		case "gray":
			return {
				text: "text-gray-700",
				bg: "bg-gray-300"
			}
		case "indigo":
			return {
				text: "text-indigo-900",
				bg: "bg-indigo-200"
			}
		case "purple":
			return {
				text: "text-purple-900",
				bg: "bg-purple-200"
			}
		default:
			return {
				text: "text-blue-900",
				bg: "bg-blue-200"
			}
	}
}

export default function ArrayTextBadge({color, children, icon}) {
	const {text, bg} = getClasses(color)
	return <span className={`relative inline-block px-3 py-1 font-semibold leading-tight max-w-max mx-auto ${text}`}>
						<span aria-hidden="true"
						      className={`absolute inset-0 bg-green-200 opacity-50 rounded-full ${bg}`}>
						</span>
						<div className="relative flex justify-between gap-x-2 items-center">
							{children}
							{icon}
						</div>
					</span>
}
