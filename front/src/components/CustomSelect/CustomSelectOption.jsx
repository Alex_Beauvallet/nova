/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React from "react"
import {CheckIcon} from "@heroicons/react/24/outline"
import {Listbox} from "@headlessui/react"
import classNames from "../../utils/functions"

export default function CustomSelectOption({option}) {
	return (
		<Listbox.Option
			key={option.id}
			className={({active}) =>
				classNames(
					active ? 'text-white bg-indigo-600' : 'text-gray-800',
					'cursor-pointer select-none relative py-2 pl-3 pr-9'
				)
			}
			value={option}>
			{({selected, active}) => (
				<>
					<div className="flex items-center">
						<div
							className="px-2 w-6 flex justify-center bg-gray-500 rounded shadow text-white font-semibold">{option.id}</div>
						<span className={classNames(selected ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}>
							{option.value}
						</span>
					</div>

					{selected ? (
						<span
							className={classNames(
								active ? 'text-white' : 'text-indigo-600',
								'absolute inset-y-0 right-0 flex items-center pr-4'
							)}>
							<CheckIcon className="h-5 w-5" aria-hidden="true"/>
						</span>
					) : null}
				</>
			)}
		</Listbox.Option>

	)
}
