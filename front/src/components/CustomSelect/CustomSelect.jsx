/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import React, {Fragment, useEffect, useState} from "react"
import {Listbox, Transition} from '@headlessui/react'
import CustomSelectOption from "./CustomSelectOption"
import {ChevronUpDownIcon} from "@heroicons/react/24/solid"


export default function CustomSelect({options, label, onChange, className, rightIcon, reset}) {
	const [selected, setSelected] = useState(options[0])

	useEffect(() => {
		if (reset) {
			setSelected(options[0])
		}
	}, [reset, options])

	return (
		<Listbox value={selected} onChange={v => {
			setSelected(v)
			onChange(v)
		}}>
			{({open}) => (
				<div className={`flex flex-col ${className}`}>
					<Listbox.Label className="block text-sm font-medium text-gray-600">{label}</Listbox.Label>
					<div className={"mt-1 flex flex-row justify-between items-center"}>
						<div className="relative w-full min-w-0">
							<Listbox.Button
								className="text-gray-800 relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-pointer focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
							<span className="flex items-center">
					              {selected.id > 0 &&
						              <span
							              className="px-2 bg-gray-500 rounded shadow text-white font-semibold">{selected.id}</span>}
								<span
									className={`${selected.id > 0 ? 'ml-3' : ''} block truncate`}>{selected.value}</span>
                             </span>
								<span
									className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                 <ChevronUpDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true"/>
                             </span>
							</Listbox.Button>
							<Transition
								show={open}
								as={Fragment}
								leave="transition ease-in duration-100"
								leaveFrom="opacity-100"
								leaveTo="opacity-0">
								<Listbox.Options
									static
									className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
									{options.map((option) => (
										option.id > 0 && <CustomSelectOption key={option.id} option={option}/>
									))}
								</Listbox.Options>
							</Transition>
						</div>
						{rightIcon}
					</div>
				</div>
			)}
		</Listbox>
	)
}
