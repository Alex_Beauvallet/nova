/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {combineReducers, configureStore} from "@reduxjs/toolkit"
import stationsReducer from "./components/Visit/Stations/stationsSlice"
import storage from 'redux-persist/lib/storage'
import {FLUSH, PAUSE, PERSIST, persistReducer, PURGE, REGISTER, REHYDRATE} from "redux-persist"
import observationsReducer from "./components/Visit/Observations/ObservationsSlice"
import eliminationsReducer from "./components/Visit/Eliminations/EliminationsSlice"
import pagesReducer from "./components/Visit/Pages/PagesSlice"
import stationsReplicaReducer from "./components/Visit/Stations/stationsReplicaSlice"
import nextVisitReducer from "./components/Visit/NextVisit/nextVisitSlice"

const reducers = combineReducers({
	stations: stationsReducer,
	stationsReplica: stationsReplicaReducer,
	observations: observationsReducer,
	eliminations: eliminationsReducer,
	nextVisit: nextVisitReducer,
	pages: pagesReducer
})

const persistConfig = {
	key: 'root',
	storage
}

const persistedReducer = persistReducer(persistConfig, reducers)


const store = configureStore({
	reducer: persistedReducer,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: {
				ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
			},
		}),
})

export default store
