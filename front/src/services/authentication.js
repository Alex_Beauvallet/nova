/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {fetchAPI} from "../utils/api"
import configData from '../../config.json'
import {useCallback} from "react"
import {useNavigate} from "react-router-dom"

const hostname = location.hostname

// noinspection JSUnresolvedVariable
/**
 * API Base URL
 * @type {string}
 */
export const baseURL = configData[hostname]?.API_BASE_URL ?? `https://api-${location.hostname}`

/**
 * Logs the user and sets the received token
 * @param data {Object} User data
 * @param token {Object} JWT token
 * @param state {Object} LoginForm states
 * @param setError {Function} Sets logging error
 * @param setLoading {Function} Sets login loading state
 */
export function login(data, token, state, setError, setLoading) {
	setLoading(true)
	fetchAPI(baseURL, "/login", {
		json: {
			username: data.email,
			password: data.password
		},
		method: "post",
		credentials: "include"
	}).then(
		/** @param r {{token: String} }*/
		r => {
			setLoading(false)
			state.setLoading(true)
			token.current = r.token
			state.setLogged(true)
			return true
		}).catch(r => {
		if (r.status === 401) {
			setLoading(false)
			setError("Identifiant ou mot de passe incorrect !")
		}
		return false
	})
}

/**
 * Gets current user and tries to refresh token if an error occurs during /me call
 * @param token {Object} JWT token
 * @param setUser {Function} Sets user info
 * @param setLoading {Function} Sets page loading state
 * @param refreshAttempt {Boolean} Tells if a refresh attempt has been done
 */
export function getUser(token, setUser, setLoading, refreshAttempt = false) {
	fetchAPI(baseURL, "/me", {
		method: "get",
	}, token.current).then(r => {
		setUser(r)
		setLoading(false)
	}).catch(() => {
		if (!refreshAttempt) {
			refresh(token).then(refreshed => {
				if (refreshed) {
					return getUser(token, setUser, setLoading, true)
				}
				setLoading(false)
			})
		}
	})
}

/**
 * Refreshes the token using the refresh token (stored in HTTPOnly cookies)
 * @param token {Object} JWT token
 * @returns {Promise<boolean>}
 */
export async function refresh(token) {
	try {
		const response = await fetchAPI(baseURL, "/token/refresh", {
			method: "get",
			credentials: "include"
		})
		token.current = response.token
		return true
	} catch (r) {
		return false
	}
}

/**
 * Custom fetch function adapted to API and JWT refresh tokens
 * If a call fails, refreshes automatically the token
 * @param baseUrl Base URL of the API
 * @param endpoint Route
 * @param options FetchOptions (method, ...)
 * @param authToken JWT
 * @param navigate History entity
 * @param refreshAttempt Indicates if a refresh attempt has been made
 * @returns {Promise<ApiResponse<*, *>>}
 */
export function internFetchAPI(baseUrl, endpoint, options, authToken, navigate, refreshAttempt = false) {
	return new Promise(async resolve => {
		try {
			resolve(await fetchAPI(baseUrl, endpoint, options, authToken.current))
		} catch (r) {
			if (r.name !== 'AbortError' && !refreshAttempt) {
				refresh(authToken).then(async refreshed => {
					if (refreshed) {
						resolve(await internFetchAPI(baseUrl, endpoint, options, authToken, navigate, true))
					} else {
						navigate("/login")
					}
				})
			}
		}
	})
}

/**
 * Custom hook used to get API function containing history entity
 * @returns {Function}
 */
export function useFetchAPI() {
	console.log("FETCH")
	const navigate = useNavigate()
	return useCallback((baseUrl, endpoint, options, authToken) => internFetchAPI(baseUrl, endpoint, options, authToken, navigate), [navigate])
}
