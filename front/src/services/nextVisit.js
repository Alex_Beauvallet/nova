/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {baseURL} from "./authentication"
import {getFormattedDuration, ISO8601Parse, mod} from "../utils/functions"
import {add, fromMinutes, normalizeTime, sum} from "pomeranian-durations"

/**
 * Gets next visit types from the API.
 * If a worksite_id is specified, the durations of each steps will be included in the response
 * @param API
 * @param token JWT Token
 * @param worksiteStatus Worksite status (surveillance, curatif...)
 * @param isInstallation Boolean which indicates if we want the visit types that are only used from installation
 * @param worksite_id ID of a worksite (used to get step durations)
 * @returns {Promise<*>}
 */
export async function getNextVisitTypes(API, token, worksiteStatus, isInstallation, worksite_id = undefined) {
	return await API(baseURL, "/visit_types", {
		method: "get",
		query: {
			"worksite_status.short_name": worksiteStatus,
			is_allowed_from_installation_only: isInstallation,
			worksite_id: worksite_id
		}
	}, token)
}

/**
 * Gets visit type durations by adding all steps of this visit and the relational time
 * The duration is normalized (ie 72 min === 1h12)
 * @param option Visit type option
 * @param localAdjustments Adjustments made locally
 * @returns {string} An ISO 8601 interval string corresponding to the duration of the visit type
 */
export function getVisitTypeDuration(option, localAdjustments) {
	const nextVisitDuration = sum([...option.steps.map(o => o.duration), ...option.steps.map(o => fromMinutes(localAdjustments[o.short_name] ?? 0))])
	return normalizeTime(add(option.relational_time, nextVisitDuration))
}

/**
 * Gets next visit date for fixed planification visit types
 * Takes into account the number of visits per year
 * @param worksite
 * @param initialDate
 * @param delayBetweenVisits
 * @returns {Date}
 */
function getFixedPlanificationNextVisitDate(worksite, initialDate, delayBetweenVisits) {
	const annualVisitDate = new Date(worksite.annual_visit_date)
	annualVisitDate.setFullYear(initialDate.getFullYear())
	annualVisitDate.setMonth(annualVisitDate.getMonth() + delayBetweenVisits)
	if (worksite.visit_per_year > 1) {
		const normalizedDelay = delayBetweenVisits / worksite.visit_per_year
		initialDate.setMonth(initialDate.getMonth() + normalizedDelay)
		if (initialDate > annualVisitDate) {
			initialDate = annualVisitDate
		} else if (annualVisitDate.getMonth() - initialDate.getMonth() > 0 && annualVisitDate.getMonth() - initialDate.getMonth() < normalizedDelay) {
			initialDate.setMonth(annualVisitDate.getMonth())
		}
	} else {
		initialDate = annualVisitDate
	}
	return initialDate
}

/**
 * Gets next visit item color
 * @param option Next visit type
 * @returns {string} The color
 */
function getNextVisitColor(option) {
	switch (option.worksite_status.short_name) {
		case "surveillance":
			return "green"
		case "curatif_exterieur_uniquement":
			return "yellow"
		case "curatif":
			return "red"
	}
}

/**
 * Gets next visit date
 * @param option Next visit type option
 * @param worksite Specific worksite
 * @returns {Date}
 */
export function getNextVisitDate(option, worksite) {
	let date = new Date()
	const {months: delayBetweenVisits} = ISO8601Parse(option.delay_between_visits)
	if (option.has_fixed_planification) {
		date = getFixedPlanificationNextVisitDate(worksite, date, delayBetweenVisits)
	} else {
		date.setMonth(date.getMonth() + delayBetweenVisits)
	}
	date.setUTCHours(0)
	date.setUTCMinutes(0)
	return date
}

/**
 * Gets next visit meta data (date, duration, color of the borders)
 * @param option
 * @param worksite
 * @param localAdjustments
 * @returns {{color: string, formattedDate: string, formattedDuration: string}}
 */
export function getNextVisitMetaData(option, worksite, localAdjustments) {
	const color = getNextVisitColor(option)
	const nextVisitDuration = getVisitTypeDuration(option, localAdjustments)
	const formattedDate = getNextVisitDate(option, worksite).toLocaleDateString('fr-FR')
	const formattedDuration = getFormattedDuration(nextVisitDuration)
	return {color, formattedDate, formattedDuration}
}

/**
 * Gets visit step types from the API.
 * If a worksite_id is specified, durations and adjustments will be included in the response
 * @param API
 * @param token JWT Token
 * @param worksite_id ID of a worksite (used to get step durations)
 * @returns {Promise<*>}
 */
export async function VisitStepTypesCharacteristics(API, token, worksite_id = undefined) {
	return await API(baseURL, "/visit_step_types", {
		method: "get",
		query: {
			worksite_id: worksite_id
		}
	}, token)
}

/**
 * Gets new worksite status in case of pre-elimination, elimination or connected SSOL
 * @param eliminations
 * @param SSOL
 * @param currentStatus
 * @returns {string|*}
 */
export function getNewWorksiteStatus(eliminations, SSOL, currentStatus) {
	const choice = eliminations?.eliminationEnd?.stateChoice ?? (eliminations?.preEliminationEnd?.stateChoice ?? undefined)
	if (choice === "sb_ssol") {
		return "surveillance"
	}
	if (choice === "sb") {
		return "curatif_exterieur_uniquement"
	}
	if (currentStatus === "surveillance" && SSOL.filter(s => s.isConnected && s.consumption >= 0).length > 0) {
		return "curatif_exterieur_uniquement"
	}
	return currentStatus
}

/**
 * Gets an array of stations to control during the next visit (in the case of complementary visit)
 * @param worksite Specific worksite
 * @param SSOLVisitStations SSOL Status during this visit
 * @returns {Array}
 */
export function getConnectedStationsToControl(worksite, SSOLVisitStations) {
	const connectedStations = SSOLVisitStations.filter(s => s.isConnected)
	const allStations = worksite.stations.filter(s => s.type.short_name === "ssol")
	const maxNumber = allStations.sort((a, b) => {
		return b.number - a.number
	})?.[0]?.number
	const stationsToControl = []

	connectedStations.forEach(s => {
		stationsToControl.push(...allStations.filter(st => st.number === s.stationNumber))
		stationsToControl.push(...allStations.filter(st => st.number === mod(s.stationNumber - 2, maxNumber) + 1))
		stationsToControl.push(...allStations.filter(st => st.number === (s.stationNumber + 1) % maxNumber))
	})

	return stationsToControl.filter((v, i, a) => a.findIndex(t => (t.id === v.id)) === i)
}
