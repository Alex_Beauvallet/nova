/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {baseURL, internFetchAPI} from "./authentication"
import store from "../store"

/**
 * Makes a call to the API to submit a front error
 * @param error {Error}
 * @param token
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function sendFrontError(error, token) {
	return await internFetchAPI(baseURL, "/front_errors", {
		method: "post",
		json: {
			errorName: error.message,
			errorStackTrace: error.stack,
			localStorage: store.getState()
		}
	}, token, null, true)
}
