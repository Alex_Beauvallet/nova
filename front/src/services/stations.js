/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

import {getLastElementArray} from "../utils/functions"
import {baseURL} from "./authentication"

/**
 * Creates an array of numbers between start and end (included)
 * @param start The number to start with
 * @param end The number to end with
 * @param length The length (optional)
 * @returns {unknown[]}
 */
function range(start, end, length = end - start + 1) {
	return Array.from({length}, (_, i) => start + i)
}

/**
 * Gets the list of stations which are not displayed in the table
 * @param worksite
 * @param type
 * @param stations
 * @param stationNumber
 * @returns Array<Object>
 */
export function getUndisplayedStationList(worksite, type, stations, stationNumber) {
	const list = worksite.stations.filter(s => s.type.short_name === type && s.is_installed && !s.is_suspended)
	const alreadyDisplayedStations = stations.map(station => station.station)
	return list.filter(s => !alreadyDisplayedStations.includes(`/stations/${s.id}`)).map(function (s) {
		return {
			value: `${s.number}` + (s.indexName ? `,${s.indexName}` : ""),
			text: `${s.number} ${s.indexName ?? ""}`
		}
	})
}

/**
 * Gets the list of all available percentages
 * @returns Array<Object>
 */
export function getPercentages() {
	const percentages = range(0, 100).filter(n => n % 5 === 0)
	return percentages.map(function (n) {
		return {
			value: n,
			text: `${n} %`
		}
	})
}

/**
 * Gets all stations of a specific type that are installed on a specific worksite with their status for each visit, ordered chronologically
 * @param worksite The worksite
 * @param type The type of the stations (ssol, sb)
 * @returns {Array}
 */
export function getInstalledStationStatuses(worksite, type) {
	return worksite.stations.filter(station => station.is_installed && !station.is_suspended && station.type.short_name === type).map(station => {
		console.log(station)
		return {
			...station,
			stationStatuses: station.stationStatuses.sort((s1, s2) => {
				if (new Date(s1.visit.scheduledAt).getTime() !== new Date(s2.visit.scheduledAt).getTime()) {
					return new Date(s1.visit.scheduledAt).getTime() - new Date(s2.visit.scheduledAt).getTime()
				}
				return s1.visit.id - s2.visit.id
			})
		}
	})
}

/**
 * Gets all stations which were connected during the last visit for a specific worksite and a specific type
 * @param worksite The worksite
 * @param type The type of the stations (ssol, sb)
 * @returns {Array}
 */
export function getLastVisitConnectedStations(worksite, type) {
	return getInstalledStationStatuses(worksite, type).filter(station => {
		return getLastElementArray(station.stationStatuses)?.isConnected
	})
}

/**
 * Gets initial common state for all stations
 * @returns {Object}
 */
export function getCommonInitialState() {
	return {
		isRemoved: false,
		isRecharged: false,
		isReplaced: false,
		isNew: false,
		isNotAccessible: false,
		isLidChanged: false,
		isDuplicated: false,
		isSuspended: false,
		isReinstalled: false
	}
}

/**
 * Sets global station state from stations object extracted from the API for a specific visit and station type
 * @param state Global station state
 * @param event_id Event ID
 * @param type Type of the station
 * @param value Stations (from the API)
 * @returns {*}
 */
export function setGlobalStationState(state, event_id, type, value) {
	const stations = value.map(station => {
		const stationStatus = getLastElementArray(station.stationStatuses)
		return {
			station: `/stations/${station.id}`,
			stationNumber: station.number,
			stationType: `/station_types/${type === 'ssol' ? '1' : '2'}`,
			isConnected: stationStatus.isConnected,
			consumption: stationStatus.consumption,
			oldConsumption: stationStatus.consumption,
			isInActivity: stationStatus.isInActivity,
			...getCommonInitialState()
		}
	})
	return {
		...state,
		[event_id]: {
			...state[event_id],
			[type]: stations
		}
	}
}

/**
 * Gets consumption history from the API
 * @param API
 * @param token JWT Token
 * @param worksite Specific worksite
 * @returns {Promise<*>}
 */
export async function getConsumptionHistory(API, token, worksite) {
	return await API(baseURL, "/worksites/{id}/consumption_history", {
		method: "get",
		params: {
			id: worksite
		}

	}, token)
}

/**
 * Gets station ID from worksite, type of station and station number
 * @param worksite Worksite
 * @param type Station type (ssol, sb)
 * @param number Station number
 * @param indexName
 * @returns {String | undefined}
 */
export function getStationID(worksite, type, number, indexName) {
	return worksite.stations.filter(s => s.type.short_name === type && s.number === parseInt(number) && (!indexName || indexName && s.indexName === indexName)).map(s => s.id)?.[0]
}

/**
 * Gets the array of all indexNames
 * @returns {string[]}
 */
export function getIndexNameArray() {
	return ["bis", "ter", "quater", "quinquies"]
}

/**
 * Gets next available index name from stationsReplica
 * @param stationsReplica StationReplica store of the station
 * @returns {string}
 */
export function getIndexNameFromStationsReplicaItem(stationsReplica) {
	const array = getIndexNameArray()
	return array[stationsReplica?.length ?? 0]
}

/**
 * Initializes StationsReplica store
 * @param stations Worksite's stations
 * @param type Stations type
 * @param event_id Event id
 * @param state Current stationsReplica state
 * @returns {*}
 */
export function initializeReplicaStations(stations, type, event_id, state) {
	const replicas = {}
	const masters = stations.filter(s => s.type.short_name === type && s.replicaStations?.length > 0)
	masters.forEach(s => {
		Object.assign(replicas, {
			[`/stations/${s.id}`]: s.replicaStations.map(r => r.indexName)
		})
	})
	const store = setGlobalStationState(state, event_id, type, [])
	store[event_id][type] = replicas
	return store
}

/**
 * Function that gets the updated value to send to redux store for a station
 * @param restrictedAttributes
 * @param station
 * @param attributeArray
 * @param locked
 * @returns {*}
 */
export function getStationValue(restrictedAttributes, station, attributeArray, locked = false) {
	const value = restrictedAttributes.reduce((prev, curr) => {
		return {
			...prev,
			[curr]: false
		}
	}, {})
	return {
		...station,
		...(!locked ? value : {}),
		...attributeArray.reduce((prev, attribute) => ({
			...prev,
			[attribute]: locked ? true : !station[attribute]
		}), {})
	}
}

/**
 * Gets restricted attributes ie all attributes without the current attribute and without all attributes from excludedAttributes array
 * @param attributes All attributes
 * @param attribute Current attribute
 * @param excludedAttributes Array of excluded attributes
 * @returns {*}
 */
export function getRestrictedAttributes(attributes, attribute, excludedAttributes) {
	return attributes.filter(d => d !== attribute && !excludedAttributes?.includes(d))
}

/**
 * Gets all removed stations
 * @param stations
 * @returns {*}
 */
export function getRemovedStations(stations) {
	return stations.filter(s => s.isRemoved).length
}
