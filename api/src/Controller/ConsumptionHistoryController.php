<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;


use App\Entity\Station;
use App\Entity\StationStatus;
use App\Entity\Visit;
use App\Entity\Worksite;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConsumptionHistoryController extends AbstractController
{
	public function __construct()
	{
	}

	public function __invoke(Worksite $data): array
	{
		$stations = $data->getStations();
		$visits = $data->getValidatedVisits();

		/**
		 * Each element of this array (corresponding to each station) contains the station number and consumption history.
		 * Consumption history is an array whose elements are station statuses for each visit during which the specific station was updated.
		 */
		$consumption = $stations->map(fn(Station $station) => [
			"number" => $station->getNumber(),
			"indexName" => $station->getIndexName(),
			"type" => $station->getType(),
			"installedDuringVisit" => $station->getInstalledDuringVisit(),
			"history" => $visits->map(fn(Visit $visit) => $visit->getStationStatuses()->filter(fn(StationStatus $status) => $status->getStation() === $station)->first()
			)
		]);

		return [
			"visits" => $visits->map(fn(Visit $visit) => [
				"id" => $visit->getId(),
				"scheduledAt" => $visit->getScheduledAt(),
				"startingEliminationPeriod" => $visit->getStartingEliminationPeriod(),
				"endingEliminationPeriod" => $visit->getEndingEliminationPeriod()
			]),
			"stations" => $consumption
		];
	}
}

