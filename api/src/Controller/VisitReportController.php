<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Controller;

use App\Entity\Visit;
use App\Service\VisitReportStationService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;

class VisitReportController extends AbstractController
{
	public function __construct(private string $gotenbergHost, private string $gotenbergPort, private HttpClientInterface $httpClient, private EntrypointLookupInterface $entrypointLookup, private VisitReportStationService $reportStationService)
	{
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws Exception
	 */
	public function __invoke(Visit $data): array
	{
		/**
		 * Creating HTML from twig template
		 */
		$html = $this->renderView('visit_report.html.twig', [
			'pdf' => true,
			'worksite' => $data->getWorksite(),
			'diverseObservations' => $data->getDiverseObservation(),
			"visitDate" => $data->getScheduledAt()->format('d-m-Y'),
			"stationInfos" => $this->reportStationService->getStationInfos($data)
		]);

		/**
		 * Multipart form data fields for Gotenberg REST API
		 */
		$formFields = [
			'files' => [(new DataPart($html, "index.html", 'text/html'))],
			'printBackground' => 'true',
			'marginTop' => '0',
			'marginBottom' => '0',
			'marginLeft' => '0',
			'marginRight' => '0',
			'paperWidth' => "8.27",
			'paperHeight' => "11.7"
		];

		$this->entrypointLookup->reset();

		/**
		 * Adding built CSS and JS files
		 */
		foreach ([...$this->entrypointLookup->getCssFiles('app'), ...$this->entrypointLookup->getJavaScriptFiles('app')] as $file) {
			$formFields['files'][] = DataPart::fromPath($this->getParameter('kernel.project_dir') . '/public/' . $file, basename($file));
		}

		$this->entrypointLookup->reset();

		/**
		 * Making request to Gotenberg REST API
		 */
		$formData = new FormDataPart($formFields);
		/** @noinspection HttpUrlsUsage */
		$pdf = $this->httpClient->request('POST', "http://$this->gotenbergHost:$this->gotenbergPort/forms/chromium/convert/html", [
			'headers' => $formData->getPreparedHeaders()->toArray(),
			'body' => $formData->bodyToIterable(),
		]);

		try {
			return [
				"pdf" => base64_encode($pdf->getContent())
			];
		} catch (ClientExceptionInterface|RedirectionExceptionInterface|TransportExceptionInterface|ServerExceptionInterface) {
			throw new Exception('PDF conversion not available');
		}
	}
}
