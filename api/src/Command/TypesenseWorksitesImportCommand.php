<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Command;


use App\Service\TypesenseService;
use Http\Client\Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Typesense\Exceptions\TypesenseClientError;

#[AsCommand(name: 'typesense:import')]
class TypesenseWorksitesImportCommand extends Command
{

	public function __construct(private TypesenseService $typesenseService, string $name = null)
	{
		parent::__construct($name);
	}

	protected function configure(): void
	{
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		try {
			$this->typesenseService->createWorksiteCollection();
		} catch (Exception|TypesenseClientError) {
			$output->writeln("Collection worksites already exist");
		} finally {
			$this->typesenseService->importAllWorksites();
			return Command::SUCCESS;
		}
	}

}
