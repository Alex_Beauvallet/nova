<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\OpenAPI;


use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\OpenApi;
use ArrayObject;

class OpenApiFactory implements OpenApiFactoryInterface
{
	public function __construct(private OpenApiFactoryInterface $decorated)
	{

	}

	public function __invoke(array $context = []): OpenApi
	{
		$openApi = $this->decorated->__invoke($context);
		/**
		 * @var PathItem $path
		 */
		foreach ($openApi->getPaths()->getPaths() as $key => $path) {
			if ($path->getGet() && $path->getGet()->getSummary() === 'hidden') {
				$openApi->getPaths()->addPath($key, $path->withGet(null));
			}
		}

		$schemas = $openApi->getComponents()->getSchemas();
		$schemas['Credentials'] = new ArrayObject([
			'type' => 'object',
			'properties' => [
				'username' => [
					'type' => 'string',
					'example' => 'john@doe.fr'
				],
				'password' => [
					'type' => 'string',
					'example' => '0000'
				]
			]
		]);
		$schemas['Refresh'] = new ArrayObject([
			'type' => 'object',
			'properties' => [
				'refresh_token' => [
					'type' => 'string',
				]
			]
		]);


		$pathItemLogin = new PathItem(
			post: new Operation(
				operationId: 'postApiLogin',
				tags: ["Auth"],
				responses: [
					'200' => [
						'description' => 'Utilisateur connecté'
					],
					'401' => [
						'description' => 'Erreur lors de la connexion'
					]
				],
				requestBody: new RequestBody(
					content: new ArrayObject([
						'application/json' => [
							'schema' => [
								'$ref' => '#/components/schemas/Credentials'
							]
						]
					])
				)
			)
		);

		$openApi->getPaths()->addPath('/login', $pathItemLogin);

		$pathItemRefresh = new PathItem(
			get: new Operation(
				operationId: 'postApiRefresh',
				tags: ["Auth"],
				responses: [
					'200' => [
						'description' => 'Token'
					],
					'401' => [
						'description' => 'Erreur lors de la connexion'
					]
				]
			)
		);

		$openApi->getPaths()->addPath('/token/refresh', $pathItemRefresh);


		return $openApi;
	}
}
