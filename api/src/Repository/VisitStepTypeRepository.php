<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Repository;

use App\Entity\VisitStepType;
use App\Entity\Worksite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VisitStepType|null find($id, $lockMode = null, $lockVersion = null)
 * @method VisitStepType|null findOneBy(array $criteria, array $orderBy = null)
 * @method VisitStepType[]    findAll()
 * @method VisitStepType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitStepTypeRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, VisitStepType::class);
	}

	// /**
	//  * @return VisitStepType[] Returns an array of VisitStepType objects
	//  */

	public function findAllWithAdjustmentsByWorksite(Worksite $worksite, bool $showDrafts = false)
	{
		$q = $this->createQueryBuilder('v')
			->addSelect('ad')
			->leftJoin('v.adjustments', 'ad', Join::WITH, 'ad.worksite = :worksite')
			->setParameter('worksite', $worksite->getId())
			->leftJoin('ad.addedDuringVisit', 'vi', Join::WITH, $showDrafts ? null : 'vi.isDraft = false');
		return $q->orderBy('v.id', 'ASC')
			->getQuery()
			->getResult();
	}
}
