<?php

namespace App\Repository;

use App\Entity\NextScheduledVisit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NextScheduledVisit|null find($id, $lockMode = null, $lockVersion = null)
 * @method NextScheduledVisit|null findOneBy(array $criteria, array $orderBy = null)
 * @method NextScheduledVisit[]    findAll()
 * @method NextScheduledVisit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NextScheduledVisitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NextScheduledVisit::class);
    }

}
