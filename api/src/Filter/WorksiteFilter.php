<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Filter;


use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Service\TypesenseService;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Http\Client\Exception;
use Psr\Log\LoggerInterface;
use Typesense\Exceptions\TypesenseClientError;

class WorksiteFilter extends AbstractFilter
{
	public function __construct(ManagerRegistry $managerRegistry, LoggerInterface $logger, private readonly TypesenseService $typesenseService)
	{
		parent::__construct($managerRegistry, $logger);
	}

	public function getDescription(string $resourceClass): array
	{
		$description = [];
		$description['q'] = [
			'property' => 'q',
			'type' => 'string',
			'required' => false,
			'strategy' => 'exact',
			'is_collection' => true
		];
		return $description;
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation $operation = null, array $context = []): void
	{
		if ($property !== "q" || $value === "") {
			return;
		}

		$worksites = $this->typesenseService->searchWorksite($value);

		$alias = $queryBuilder->getRootAliases()[0];
		$parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters
		$queryBuilder
			->andWhere(sprintf('%s.%s IN (:%s)', $alias, "id", $parameterName))
			->setParameter($parameterName, $worksites);
	}
}

