<?php /*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */
/** @noinspection PhpUnusedPrivateMethodInspection */

/** @noinspection PhpUnusedParameterInspection */


namespace App\Service;


use App\Entity\StationType;
use App\Entity\VisitStepType;
use App\Entity\VisitStepTypeAdjustment;
use App\Entity\VisitType;
use App\Entity\Worksite;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class StepDurationCalculationService
{
	/**
	 * @var array|string[][]
	 * Correspondence between visit step type and functions to calculate durations
	 */
	private array $functionCorrespondences = [
		"ssol_all" => ["calculateStationDuration", "ssol"],
		"ssol_connec" => ["calculateStationDuration", "ssol", true],
		"sb" => ["calculateStationDuration", "sb"],
		"cb" => ["getWorksiteAttribute", "builtControlDuration"],
		"installation" => ["getTotalDuration"],
	];

	public function __construct(private EntityManagerInterface $entityManager)
	{
	}

	/**
	 * @param VisitType $visitType The visit type
	 * @param Worksite $worksite The concerned worksite
	 * @param DateInterval $totalDuration
	 * Calculates the duration of all steps of a visit type
	 */
	public function calculateStepsDuration(VisitType $visitType, Worksite $worksite, DateInterval $totalDuration): VisitType
	{
		$initialVisitTypeSteps = $visitType->getSteps();
		$visitType->steps = new ArrayCollection([]);
		foreach ($initialVisitTypeSteps as $stepType) {
			$updatedVisitStepType = clone $stepType;
			$updatedVisitStepType->setDuration($this->calculateStepDuration($stepType, $worksite, $totalDuration));
			$visitType->steps->add($updatedVisitStepType);
		}
		return $visitType;
	}

	/**
	 * @param VisitStepType $stepType The type of the visit
	 * @param Worksite $worksite The concerned worksite
	 * @param DateInterval $totalDuration The total duration of the visit
	 * @return DateInterval
	 * Calculates the duration of a step of a visit type
	 */
	private function calculateStepDuration(VisitStepType $stepType, Worksite $worksite, DateInterval $totalDuration): DateInterval
	{
		[$originalDuration, $adjustmentDuration] = $this->getCharacteristicDurations($stepType, $worksite, $totalDuration);
		if ($originalDuration instanceof DateInterval) {
			return $this->addDateIntervals($originalDuration, $adjustmentDuration);
		}
		return DateInterval::createFromDateString('0 min');
	}

	/**
	 * Gets characteristic durations of a specific visit step type for a specific worksite (original duration and adjustments)
	 * @param VisitStepType $stepType
	 * @param Worksite $worksite
	 * @param DateInterval $totalDuration
	 * @return array|null[]
	 */
	public function getCharacteristicDurations(VisitStepType $stepType, Worksite $worksite, DateInterval $totalDuration): array
	{
		$correspondence = $this->functionCorrespondences[$stepType->getShortName()] ?? null;
		if ($correspondence) {
			$adjustments = $stepType->getAdjustmentsFromWorksite($worksite);
			$originalDuration = $this->{$correspondence[0]}($worksite, $totalDuration, ...array_slice($correspondence, 1));
			$adjustmentDuration = $this->addDateIntervals(...(array_map(fn(VisitStepTypeAdjustment $a) => $a->getDuration(), $adjustments->toArray())));
			return [$originalDuration, $adjustmentDuration];
		}
		return [null, null];
	}

	/**
	 * Combine a number of DateIntervals into 1
	 * @return DateInterval
	 */
	public static function addDateIntervals(): DateInterval
	{
		$reference = new DateTimeImmutable;
		$endTime = clone $reference;

		foreach (func_get_args() as $dateInterval) {
			$endTime = $endTime->add($dateInterval);
		}

		return $reference->diff($endTime);
	}

	/**
	 * @param Worksite $worksite The worksite
	 * @param DateInterval $totalDuration Total duration of the visit
	 * @param string $station Station type
	 * @return DateInterval
	 * Calculate the duration of station control
	 */
	private function calculateStationDuration(Worksite $worksite, DateInterval $totalDuration, string $station, bool $isUnitAsked = false): DateInterval
	{
		$stationType = $this->entityManager->getRepository(StationType::class)->findOneBy(["short_name" => $station]);
		if ($stationType instanceof StationType) {
			$unitDuration = $stationType->getControlDuration();
			if ($isUnitAsked) {
				return DateInterval::createFromDateString($unitDuration . "min");
			}
			$duration = DateInterval::createFromDateString($unitDuration * $this->getWorksiteAttribute($worksite, $totalDuration, $station . "Number") . "min");
			return $this->recalculate($duration);
		}
		return DateInterval::createFromDateString('0 min');
	}

	/**
	 * @param Worksite $worksite The worksite
	 * @param DateInterval $totalDuration Total duration of the visit
	 * @return int|DateInterval
	 * Gets an attribute from the worksite
	 */
	private function getWorksiteAttribute(Worksite $worksite, DateInterval $totalDuration, string $attribute): int|DateInterval
	{
		$getter = "get" . ucfirst($attribute);
		return $worksite->{$getter}();
	}

	/**
	 * @param DateInterval $duration The duration
	 * @return DateInterval
	 * Recalculates the DateInterval so that fields are balanced
	 */
	public static function recalculate(DateInterval $duration): DateInterval
	{
		$from = new DateTime;
		$to = clone $from;
		$to = $to->add($duration);
		$diff = $from->diff($to);
		foreach ($diff as $k => $v) $duration->$k = $v;
		return $duration;
	}

	private function getTotalDuration(Worksite $worksite, DateInterval $totalDuration): DateInterval
	{
		return $totalDuration;
	}
}

