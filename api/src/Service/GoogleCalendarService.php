<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service;


use App\Entity\Contact;
use App\Entity\NextScheduledVisit;
use App\Entity\User;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Google\Service\Calendar\Event;
use Google\Service\Calendar\Events;
use Google_Service_Calendar;
use Symfony\Component\Security\Core\User\UserInterface;

class GoogleCalendarService extends GoogleAbstractService
{

	public function __construct(string $env, string $clientID, string $clientSecret, string $tokenPath, string $scope, private readonly string $novaCalendarID, private readonly EntityManagerInterface $entityManager)
	{
		parent::__construct($env, $clientID, $clientSecret, $tokenPath, $scope);
	}

	/**
	 * Gets all the scheduled events in a user's calendar during a specific day.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return Events|null The corresponding events
	 */
	public function getScheduledEvents(?UserInterface $user, DateTime $date): ?Events
	{
		return $this->listEvents($user, $date, []);
	}

	/**
	 * Gets all the scheduled events in a user's calendar during a specific day with options.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return Events|null The corresponding events
	 */
	private function listEvents(?UserInterface $user, DateTime $date, array $options): ?Events
	{
		if ($user instanceof User) {
			$maxTime = $date->setTime(23, 59);
			return $this->listEventsFromSpecificDay($user, $date, array_merge([
				"timeMax" => $maxTime->format(DateTimeInterface::RFC3339)
			], $options));
		}
		return null;
	}

	/**
	 * Gets all the scheduled events in a user's calendar from a specific day with options.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return Events|null The corresponding events
	 */
	private function listEventsFromSpecificDay(?UserInterface $user, DateTime $date, array $options): ?Events
	{
		$service = new Google_Service_Calendar($this->client);
		if ($user instanceof User) {
			$minTime = $date->setTime(0, 0);
			return $service->events->listEvents($user->getCalendarId(), array_merge([
				"timeMin" => $minTime->format(DateTimeInterface::RFC3339),
			], $options));
		}
		return null;
	}

	/**
	 * Gets all events of a worksite during a specific date or gets the next visits of the worksite as from the specified day
	 * @param string $code Worksite's code
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @param bool $nextVisit Boolean which indicates if we look for the next visit after the specified date
	 * @return ArrayCollection
	 */
	public function getWorksiteEvents(string $code, DateTime $date, bool $nextVisit = false): ArrayCollection
	{
		$events = [];
		$users = $this->entityManager->getRepository(User::class)->findAll();
		foreach ($users as $user) {
			if ($user instanceof UserInterface) {
				if ($nextVisit) {
					$items = $this->listEventsFromSpecificDay($user, $date, [
						"q" => $code
					]);
				} else {
					$items = $this->getWorksiteUserEvents($user, $code, $date);
				}
				$events = [...$events, ...$items];
			}
		}
		return new ArrayCollection($events);
	}

	/**
	 * Gets all events of a worksite during a specific day for a specific user.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param string $code Worksite's code
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return array Events of a worksite during a specific day for a specific user.
	 */
	public function getWorksiteUserEvents(?UserInterface $user, string $code, DateTime $date): array
	{
		return array_filter($this->listEvents($user, $date, [])["items"], fn($event) => $event instanceof Event && str_contains($event->getDescription(), $code));
	}

	/**
	 * Gets the specified event from user's calendar
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param string $eventId Google Calendar Event ID
	 * @return Event|null
	 */
	public function getEvent(?UserInterface $user, string $eventId): ?Event
	{
		$service = new Google_Service_Calendar($this->client);
		if ($user instanceof User) {
			return $service->events->get($user->getCalendarId(), $eventId);
		}
		return null;
	}

	/**
	 * Creates an event on Google Calendar thanks to a Visit
	 * @throws Exception
	 */
	public function createEvent(NextScheduledVisit $visit): Event
	{
		$timeZone = "Europe/Paris";
		$service = new Google_Service_Calendar($this->client);
		$address = $visit->getWorksite()->getAddress();
		$beginDatetime = DateTime::createFromInterface($visit->getScheduledAt())->setTimezone(new DateTimeZone($timeZone))->setTime(18, 0);
		$event = new Event([
			"summary" => $this->getEventSummary($visit),
			'location' => $address->getStreet() . " - " . $address->getPostalCode() . " " . $address->getCity(),
			'description' => $visit->getWorksite()->getCode(),
			'start' => array(
				'dateTime' => $beginDatetime->format(DateTimeInterface::RFC3339),
				'timeZone' => $timeZone,
			),
			'end' => array(
				'dateTime' => $beginDatetime->add($visit->getEstimatedDuration())->format(DateTimeInterface::RFC3339),
				'timeZone' => $timeZone,
			),
		]);
		return $service->events->insert($this->novaCalendarID, $event);
	}

	/**
	 * Gets the summary of the new Google Calendar Event
	 * @param NextScheduledVisit $visit The visit
	 * @return string
	 */
	private function getEventSummary(NextScheduledVisit $visit): string
	{
		return $visit->getWorksite()->getName() . " " . $visit->getType()->getCode() . " - " . $this->getContact($visit);
	}

	/**
	 * Gets phone numbers of the worksite, separated by a comma
	 * @param NextScheduledVisit $visit Visit The visit
	 * @return string
	 */
	private function getContact(NextScheduledVisit $visit): string
	{
		return join(",", $visit->getWorksite()->getContacts()->map(fn(Contact $c) => $c->getPhoneNumber())->toArray());
	}
}

