<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Service\Import;


use App\Entity\Import\PDFStation;
use App\Entity\Import\PDFVisitInfo;
use App\Entity\StationStatus;
use App\Entity\StationType;
use App\Entity\Visit;
use DateInterval;
use mikehaertl\pdftk\Pdf;

class PDFImportService
{

	const FIELD_NAME = 'FieldName';
	const FIELD_VALUE = 'FieldValue';

	private array $regexpCorrespondance = [
		"ssol" => '/station_([1-9]+)/',
		"sb" => '/station_sb_([1-9]+)/',
	];

	/**
	 * Correspondence between client form fields and station types, methods to call on StationStatus entity and value to set.
	 * @var array
	 */
	private array $clientFunctionStationCorrespondance = [
		"maintint1" => ["type" => "sb", "function" => "setIsRecharged", "value" => true],
		"maintint2" => ["type" => "sb", "function" => "setIsNew", "value" => true],
		"maintint3" => ["type" => "sb", "function" => "setIsRemoved", "value" => true],
		"entext1" => ["type" => "ssol", "function" => "setIsReplaced", "value" => true],
		"entext2" => ["type" => "ssol", "function" => "setIsLidChanged", "value" => true],
		"entext3" => ["type" => "ssol", "function" => "setIsRecharged", "value" => true],
		"entext4" => ["type" => "ssol", "function" => "setIsNew", "value" => true],
		"inacc" => ["type" => "ssol", "function" => "setIsNotAccessible", "value" => true],
	];

	/**
	 * Reads information from PDFs (client and intern PDF) for a specific visit
	 * @param $internPath
	 * @param $clientPath
	 * @param Visit $visit
	 * @return null[]|null
	 * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
	 */
	public function readFromPDF($internPath, $clientPath, Visit $visit): ?array
	{
		$information = [
			"stations" => null,
			"visit" => null
		];
		if ($internArray = $this->getDataFromPDF($internPath)) {
			$connectedStations = array_merge($this->getConnectedStations($internArray, "ssol"), $this->getConnectedStations($internArray, "sb"));
			if ($clientArray = $this->getDataFromPDF($clientPath)) {
				$information["stations"] = $this->getStationMovements($clientArray, $connectedStations, $visit);
				$information["visit"] = $this->getVisitInfo($clientArray, $internArray);
			} else {
				$information["stations"] = $connectedStations;
			}
		}
		return $information;
	}

	/**
	 * Gets data from the specified PDF
	 * @param string $path Path of the PDF
	 * @return array|null
	 */
	private function getDataFromPDF(string $path): ?array
	{
		$pdf = new Pdf($path);
		$dataFields = $pdf->getDataFields();
		if ($dataFields === false) {
			$error = $pdf->getError();
			dump($error);
			return null;
		} else {
			return $dataFields->getArrayCopy();
		}
	}

	/**
	 * Gets connected stations of a specific type
	 * @param array $data PDF Data
	 * @param $type string Station type
	 * @return array
	 */
	private function getConnectedStations(array $data, string $type): array
	{
		$originalStations = array_filter($data, fn($field) => preg_match($this->regexpCorrespondance[$type], $field[self::FIELD_NAME]) && isset($field[self::FIELD_VALUE]) && $field[self::FIELD_VALUE] > 0);
		return array_map(fn($station) => $this->hydratePDFStation($data, $station, $type), $originalStations);
	}

	/**
	 * Hydrates PDF Station
	 * @param array $data PDF Data
	 * @param array $station Station data
	 * @param string $type Station type
	 * @return PDFStation
	 */
	private function hydratePDFStation(array $data, array $station, string $type): PDFStation
	{
		$matches = [];
		preg_match($this->regexpCorrespondance[$type], $station[self::FIELD_NAME], $matches);
		$station = (new PDFStation())
			->setId($matches[1])
			->setNumber($station[self::FIELD_VALUE])
			->setType((new StationType())->setShortName($type));
		return $this->hydrateWithPercentage($station, $data);
	}

	/**
	 * Hydrates a station with consumption percentage (and activity)
	 * @param PDFStation $station Station
	 * @param array $data PDF data
	 * @return PDFStation
	 */
	private function hydrateWithPercentage(PDFStation $station, array $data): PDFStation
	{
		$fieldNames = $this->getFieldsName($station->getType()->getShortName(), $station->getId());
		$percentage = $this->findValueInPDF($data, $fieldNames[0]);
		$activity = $this->findValueInPDF($data, $fieldNames[1]);
		return $station->setStationStatus((new StationStatus())
			->setConsumption(intval($percentage))
			->setIsConnected(true)
			->setIsRecharged(false)
			->setIsRemoved(false)
			->setIsReplaced(false)
			->setIsNew(false)
			->setIsNotAccessible(false)
			->setIsLidChanged(false)
			->setIsDuplicated(false)
			->setIsSuspended(false)
			->setIsReinstalled(false)
			->setIsInActivity($activity === 'Oui'));
	}

	/**
	 * Gets PDF field name corresponding to station type and id
	 * @param $type String of the station
	 * @param $id String of the station
	 * @return array|null
	 */
	private function getFieldsName(string $type, string $id): ?array
	{
		switch ($type) {
			case "ssol":
				return ["station_pour_$id", "activite_$id"];
			case "sb":
				return ["station_sb_pour_$id", "activite_sb_$id"];
		}
		return null;
	}

	/**
	 * Finds an element in PDF Data
	 * @param array $array Original array
	 * @param string $slug Slug of the element
	 * @return mixed|null
	 */
	private function findValueInPDF(array $array, string $slug): string|null
	{
		$element = array_filter($array, fn($fields) => $fields[self::FIELD_NAME] === $slug);
		if (count($element) > 0) {
			$first = $element[array_key_first($element)];
			if (isset($first[self::FIELD_VALUE])) {
				return $first[self::FIELD_VALUE];
			}
		}
		return null;
	}

	/**
	 * Gets all station movements (connection, consumption, recharging, replacing etc..) for a specific visit.
	 * @param array $data PDF Data
	 * @param array $stations Station array
	 * @param Visit $visit The specified visit
	 * @return array
	 */
	private function getStationMovements(array $data, array $stations, Visit $visit): array
	{
		foreach ($this->clientFunctionStationCorrespondance as $slug => $corr) {
			$list = $this->findValueInPDF($data, $slug);
			if ($list === null) {
				continue;
			}
			$array = $this->commaStringToArray($list);
			foreach ($array as $stationNumber) {
				$function = $corr["function"];
				$value = $corr["value"];
				$station = $this->isStationInArray($stations, $stationNumber, $corr["type"]);
				if ($station === false) {
					$station = $this->hydrateNotConnectedStation($stationNumber, $corr);
					$stations[] = $station;
				}
				$station->getStationStatus()->$function($value);
				if ($station->getStationStatus()->getIsNew()) {
					$station->setInstalledDuringVisit($visit);
				}
			}
		}
		return $stations;
	}

	/**
	 * Transforms a comma-separated string into an array of integers. Ex : "1,2,3,4" -> [1,2,3,4]
	 * @param string $string Original string
	 * @return array
	 */
	private function commaStringToArray(string $string): array
	{
		return array_map(fn($e) => intval($e), explode(",", $string));
	}

	/**
	 * Checks if a station is in stations array.
	 * @param $stations
	 * @param $number
	 * @param $type
	 * @return PDFStation|bool Returns the station if it was found and false otherwise.
	 */
	private function isStationInArray($stations, $number, $type): PDFStation|bool
	{
		$found = (array_filter($stations, fn(PDFStation $station) => $station->getNumber() === $number && $station->getType()->getShortName() === $type));
		if (count($found) > 0) {
			return $found[array_key_first($found)];
		}
		return false;
	}

	/**
	 * Hydrates not connected station
	 * @param int $stationNumber Station number
	 * @param array $corr Correspondence array (function, value)
	 * @return PDFStation
	 */
	private function hydrateNotConnectedStation(int $stationNumber, array $corr): PDFStation
	{
		return (new PDFStation())
			->setType((new StationType())->setShortName($corr["type"]))
			->setNumber($stationNumber)
			->setStationStatus((new StationStatus())
				->setIsInActivity(false)
				->setIsReplaced(false)
				->setIsRemoved(false)
				->setIsRecharged(false)
				->setIsConnected(false)
				->setConsumption(0)
				->setIsNew(false)
				->setIsNotAccessible(false)
				->setIsLidChanged(false)
				->setIsDuplicated(false)
				->setIsSuspended(false)
				->setIsReinstalled(false)
			);
	}

	/**
	 * Get all visit info (observations, effective duration...)
	 * @param array $clientData Client PDF Data
	 * @param array $internData Intern PDF Data
	 * @return PDFVisitInfo
	 */
	private function getVisitInfo(array $clientData, array $internData): PDFVisitInfo
	{
		$checkbox = $this->findValueInPDF($clientData, 'radio');
		$obs1 = $this->findValueInPDF($clientData, 'Text15');
		$obs2 = $this->findValueInPDF($clientData, 'obs2');
		$obsInterne = $this->findValueInPDF($internData, 'commtech');
		$effectiveDurationHours = $this->findValueInPDF($internData, 'dureeh') ?? "0";
		$effectiveDurationMinutes = $this->findValueInPDF($internData, 'dureemin') ?? "0";
		$effectiveDuration = "$effectiveDurationHours hours + $effectiveDurationMinutes min";
		$pdfVisitInfo = (new PDFVisitInfo())
			->setIsEliminationBegin($checkbox === "2")
			->setIsEliminationEnd($checkbox === "1" || $checkbox === "3")
			->setDiverseObservations(trim($obs1 . $obs2))
			->setInternObservations(trim($obsInterne));
		if ($effectiveDuration !== "0 hours + 0 min") {
			$pdfVisitInfo->setEffectiveDuration(DateInterval::createFromDateString($effectiveDuration));
		}
		return $pdfVisitInfo;
	}
}

