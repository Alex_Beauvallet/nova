<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;


use ApiPlatform\Metadata\ApiResource;
use App\Repository\StationStatusRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ['groups' => ['worksite', 'worksite_summary', 'scheduled_visit']])]
#[ORM\Entity(repositoryClass: StationStatusRepository::class)]
class StationStatus
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean')]
	private $isConnected;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'integer')]
	private $consumption;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\ManyToOne(targetEntity: Visit::class, inversedBy: 'stationStatuses')]
	#[ORM\JoinColumn(nullable: false)]
	private $visit;

	#[Groups(['write:visit'])]
	#[ORM\ManyToOne(targetEntity: Station::class, cascade: ['persist'], inversedBy: 'stationStatuses')]
	#[ORM\JoinColumn(nullable: false)]
	private $station;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean')]
	private $isInActivity;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isRemoved;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isRecharged;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isReplaced;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isNew;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isNotAccessible;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isLidChanged;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isDuplicated;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isSuspended;

	#[Groups(['worksite_summary', 'write:visit'])]
	#[ORM\Column(type: 'boolean', nullable: false)]
	private $isReinstalled;

	#[Groups(['write:visit'])]
	private ?int $stationNumber = null;

	#[Groups(['write:visit'])]
	private ?Station $masterStation = null;

	#[Groups(['write:visit'])]
	private ?string $indexName = null;

	#[Groups(['write:visit'])]
	private ?StationType $stationType = null;


	/**
	 * @return bool
	 */
	#[Groups(['worksite_summary'])]
	public function getIsDraft(): bool
	{
		return $this->getVisit()->getIsDraft();
	}

	public function getVisit(): ?Visit
	{
		return $this->visit;
	}

	public function setVisit(?Visit $visit): self
	{
		$this->visit = $visit;

		return $this;
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getIsConnected(): ?bool
	{
		return $this->isConnected;
	}

	public function setIsConnected(bool $isConnected): self
	{
		$this->isConnected = $isConnected;

		return $this;
	}

	public function getConsumption(): ?int
	{
		return $this->consumption;
	}

	public function setConsumption(int $consumption): self
	{
		$this->consumption = $consumption;

		return $this;
	}

	public function getStation(): ?Station
	{
		return $this->station;
	}

	public function setStation(?Station $station): self
	{
		$this->station = $station;

		return $this;
	}

	public function getIsInActivity(): ?bool
	{
		return $this->isInActivity;
	}

	public function setIsInActivity(bool $isInActivity): self
	{
		$this->isInActivity = $isInActivity;

		return $this;
	}

	public function getIsRemoved(): ?bool
	{
		return $this->isRemoved;
	}

	public function setIsRemoved(?bool $isRemoved): self
	{
		$this->isRemoved = $isRemoved;

		return $this;
	}

	public function getIsRecharged(): ?bool
	{
		return $this->isRecharged;
	}

	public function setIsRecharged(?bool $isRecharged): self
	{
		$this->isRecharged = $isRecharged;

		return $this;
	}

	public function getIsReplaced(): ?bool
	{
		return $this->isReplaced;
	}

	public function setIsReplaced(?bool $isReplaced): self
	{
		$this->isReplaced = $isReplaced;

		return $this;
	}

	public function getIsNew(): ?bool
	{
		return $this->isNew;
	}

	public function setIsNew(?bool $isNew): self
	{
		$this->isNew = $isNew;

		return $this;
	}

	public function getIsNotAccessible(): ?bool
	{
		return $this->isNotAccessible;
	}

	public function setIsNotAccessible(?bool $isNotAccessible): self
	{
		$this->isNotAccessible = $isNotAccessible;

		return $this;
	}

	public function getIsLidChanged(): ?bool
	{
		return $this->isLidChanged;
	}

	public function setIsLidChanged(?bool $isLidChanged): self
	{
		$this->isLidChanged = $isLidChanged;

		return $this;
	}

	public function getIsDuplicated(): ?bool
	{
		return $this->isDuplicated;
	}

	public function setIsDuplicated(?bool $isDuplicated): self
	{
		$this->isDuplicated = $isDuplicated;

		return $this;
	}

	public function getIsSuspended(): ?bool
	{
		return $this->isSuspended;
	}

	public function setIsSuspended(?bool $isSuspended): self
	{
		$this->isSuspended = $isSuspended;

		return $this;
	}

	public function getStationNumber(): ?int
	{
		return $this->stationNumber;
	}

	public function setStationNumber(?int $stationNumber): self
	{
		$this->stationNumber = $stationNumber;

		return $this;
	}

	public function getMasterStation(): ?Station
	{
		return $this->masterStation;
	}

	public function setMasterStation(?Station $masterStation): self
	{
		$this->masterStation = $masterStation;

		return $this;
	}

	public function getIndexName(): ?string
	{
		return $this->indexName;
	}

	public function setIndexName(?string $indexName): self
	{
		$this->indexName = $indexName;

		return $this;
	}

	public function getStationType(): ?StationType
	{
		return $this->stationType;
	}

	public function setStationType(?StationType $stationType): self
	{
		$this->stationType = $stationType;

		return $this;
	}

	public function getIsReinstalled(): ?bool
	{
		return $this->isReinstalled;
	}

	public function setIsReinstalled(?bool $isReinstalled): self
	{
		$this->isReinstalled = $isReinstalled;

		return $this;
	}
}

