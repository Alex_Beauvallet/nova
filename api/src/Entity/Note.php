<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\NoteRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ['groups' => ['worksite']])]
#[ORM\Entity(repositoryClass: NoteRepository::class)]
class Note
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'text')]
	private $content;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'datetime', nullable: true)]
	private $created_at;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'boolean')]
	private $is_visible;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'boolean')]
	private $is_punctual;

	#[ORM\ManyToOne(targetEntity: Worksite::class, inversedBy: 'notes')]
	#[ORM\JoinColumn(nullable: false)]
	private $worksite;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(string $content): self
	{
		$this->content = $content;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->created_at;
	}

	public function setCreatedAt(DateTimeInterface $created_at): self
	{
		$this->created_at = $created_at;

		return $this;
	}

	public function getIsVisible(): ?bool
	{
		return $this->is_visible;
	}

	public function setIsVisible(bool $is_visible): self
	{
		$this->is_visible = $is_visible;

		return $this;
	}

	public function getIsPunctual(): ?bool
	{
		return $this->is_punctual;
	}

	public function setIsPunctual(bool $is_punctual): self
	{
		$this->is_punctual = $is_punctual;

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}
}

