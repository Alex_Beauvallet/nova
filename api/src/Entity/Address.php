<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ['groups' => ['worksite']])]
#[ORM\Entity(repositoryClass: AddressRepository::class)]
class Address
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'string', length: 255)]
	private $street;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'integer')]
	private $postal_code;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'string', length: 255)]
	private $city;

	#[ORM\OneToMany(mappedBy: 'address', targetEntity: Worksite::class)]
	private $worksites;

	public function __construct()
	{
		$this->worksites = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getStreet(): ?string
	{
		return $this->street;
	}

	public function setStreet(string $street): self
	{
		$this->street = $street;

		return $this;
	}

	public function getPostalCode(): ?int
	{
		return $this->postal_code;
	}

	public function setPostalCode(int $postal_code): self
	{
		$this->postal_code = $postal_code;

		return $this;
	}

	public function getCity(): ?string
	{
		return $this->city;
	}

	public function setCity(string $city): self
	{
		$this->city = $city;

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->setAddress($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			// set the owning side to null (unless already changed)
			if ($worksite->getAddress() === $this) {
				$worksite->setAddress(null);
			}
		}

		return $this;
	}
}
