<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;


use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Controller\VisitReportController;
use App\State\VisitProcessor;
use App\State\VisitValidationProcessor;
use DateInterval;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
	operations: [
		new Get(
			uriTemplate: '/visits/{id}/report',
			controller: VisitReportController::class,
			security: 'is_granted("ROLE_USER")',
		),
		new Post(
			denormalizationContext: ['groups' => ['write:visit']],
			security: 'is_granted("ROLE_USER")',
			processor: VisitProcessor::class
		),
		new Put(
			uriTemplate: '/visits/{id}/validation',
			denormalizationContext: ['groups' => ['write:validate_visit']],
			security: 'is_granted("ROLE_USER")',
			processor: VisitValidationProcessor::class
		)])]
#[ApiFilter(SearchFilter::class, properties: ['worksite' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['scheduled_at' => 'ASC'])]
#[ORM\Entity(repositoryClass: 'App\Repository\VisitRepository')]
class Visit
{
	#[Groups('scheduled_visit')]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups(['scheduled_visit', 'write:visit'])]
	#[ORM\Column(type: 'datetime')]
	private $scheduledAt;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'datetime')]
	private $createdAt;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'datetime')]
	private $updatedAt;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'dateinterval')]
	private $estimatedDuration;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'dateinterval')]
	private $effectiveDuration;

	#[Groups(['scheduled_visit', 'write:visit'])]
	#[ORM\Column(type: 'boolean')]
	private $isPending;

	#[Groups(['scheduled_visit', 'write:validate_visit'])]
	#[ORM\Column(type: 'boolean')]
	private $isCompleted = false;

	#[Groups(['write:visit'])]
	#[ORM\ManyToOne(targetEntity: Worksite::class, inversedBy: 'visits')]
	#[ORM\JoinColumn(nullable: false)]
	private $worksite;

	#[Groups('write:visit')]
	#[ORM\OneToMany(mappedBy: 'visit', targetEntity: StationStatus::class, cascade: ['persist'], orphanRemoval: true)]
	private $stationStatuses;

	#[Groups('write:visit')]
	#[ORM\ManyToOne(targetEntity: VisitType::class, inversedBy: 'visits')]
	#[ORM\JoinColumn(nullable: true)]
	private $type;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'string', length: 255)]
	private $eventId;

	#[Groups('write:visit')]
	#[ORM\OneToMany(mappedBy: 'installedDuringVisit', targetEntity: Station::class)]
	private $installedStations;

	#[Groups('write:visit')]
	#[ORM\OneToOne(mappedBy: 'startVisit', targetEntity: EliminationPeriod::class, cascade: ['persist', 'remove'])]
	private $startingEliminationPeriod;

	#[Groups('write:visit')]
	#[ORM\OneToOne(mappedBy: 'endVisit', targetEntity: EliminationPeriod::class, cascade: ['persist', 'remove'])]
	private $endingEliminationPeriod;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'text', nullable: false)]
	private $diverseObservation = "";

	#[Groups('write:visit')]
	#[ORM\Column(type: 'text', nullable: false)]
	private $internObservation = "";

	#[Groups('write:visit')]
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $clientReportFileID;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $InternReportFileID;

	#[Groups('write:visit')]
	#[ORM\OneToMany(mappedBy: 'addedDuringVisit', targetEntity: VisitStepTypeAdjustment::class, cascade: ['persist'], orphanRemoval: true)]
	private $visitStepTypeAdjustments;

	#[Groups('write:visit')]
	private array $durationAdjustments = [];

	#[Groups(['write:visit'])]
	#[ORM\OneToOne(mappedBy: 'previousVisit', targetEntity: NextScheduledVisit::class, cascade: ['persist', 'remove'])]
	private $nextScheduledVisit;

	#[Groups(['write:validate_visit'])]
	#[ORM\Column(type: 'boolean')]
	private bool $isDraft = true;


	public function __construct()
	{
		$this->stationStatuses = new ArrayCollection();
		$this->installedStations = new ArrayCollection();
		$this->visitStepTypeAdjustments = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Visit
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getScheduledAt(): ?DateTimeInterface
	{
		return $this->scheduledAt;
	}

	public function setScheduledAt(DateTimeInterface $scheduledAt): self
	{
		$this->scheduledAt = $scheduledAt;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->createdAt;
	}

	public function setCreatedAt(DateTimeInterface $createdAt): self
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getUpdatedAt(): ?DateTimeInterface
	{
		return $this->updatedAt;
	}

	public function setUpdatedAt(DateTimeInterface $updatedAt): self
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	public function getEstimatedDuration(): ?DateInterval
	{
		return $this->estimatedDuration;
	}

	public function setEstimatedDuration(DateInterval $estimatedDuration): self
	{
		$this->estimatedDuration = $estimatedDuration;

		return $this;
	}

	public function getEffectiveDuration(): ?DateInterval
	{
		return $this->effectiveDuration;
	}

	public function setEffectiveDuration(DateInterval $effectiveDuration): self
	{
		$this->effectiveDuration = $effectiveDuration;

		return $this;
	}

	public function getIsPending(): ?bool
	{
		return $this->isPending;
	}

	public function setIsPending(bool $isPending): self
	{
		$this->isPending = $isPending;

		return $this;
	}

	public function getIsCompleted(): ?bool
	{
		return $this->isCompleted;
	}

	public function setIsCompleted(bool $isCompleted): self
	{
		$this->isCompleted = $isCompleted;

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getStationStatuses(): Collection
	{
		return $this->stationStatuses;
	}

	public function addStationStatus(StationStatus $stationStatus): self
	{
		if (!$this->stationStatuses->contains($stationStatus)) {
			$this->stationStatuses[] = $stationStatus;
			$stationStatus->setVisit($this);
		}

		return $this;
	}

	public function removeStationStatus(StationStatus $stationStatus): self
	{
		if ($this->stationStatuses->removeElement($stationStatus)) {
			// set the owning side to null (unless already changed)
			if ($stationStatus->getVisit() === $this) {
				$stationStatus->setVisit(null);
			}
		}

		return $this;
	}

	public function getType(): ?VisitType
	{
		return $this->type;
	}

	public function setType(?VisitType $type): self
	{
		$this->type = $type;

		return $this;
	}

	public function getEventId(): ?string
	{
		return $this->eventId;
	}

	public function setEventId(string $eventId): self
	{
		$this->eventId = $eventId;

		return $this;
	}

	/**
	 * @return Collection|Station[]
	 */
	public function getInstalledStations(): Collection
	{
		return $this->installedStations;
	}

	public function addInstalledStation(Station $installedStation): self
	{
		if (!$this->installedStations->contains($installedStation)) {
			$this->installedStations[] = $installedStation;
			$installedStation->setInstalledDuringVisit($this);
		}

		return $this;
	}

	public function removeInstalledStation(Station $installedStation): self
	{
		if ($this->installedStations->removeElement($installedStation)) {
			// set the owning side to null (unless already changed)
			if ($installedStation->getInstalledDuringVisit() === $this) {
				$installedStation->setInstalledDuringVisit(null);
			}
		}

		return $this;
	}

	public function getStartingEliminationPeriod(): ?EliminationPeriod
	{
		return $this->startingEliminationPeriod;
	}

	public function setStartingEliminationPeriod(EliminationPeriod $startingEliminationPeriod): self
	{
		// set the owning side of the relation if necessary
		if ($startingEliminationPeriod->getStartVisit() !== $this) {
			$startingEliminationPeriod->setStartVisit($this);
		}

		$this->startingEliminationPeriod = $startingEliminationPeriod;

		return $this;
	}

	public function getEndingEliminationPeriod(): ?EliminationPeriod
	{
		return $this->endingEliminationPeriod;
	}

	public function setEndingEliminationPeriod(?EliminationPeriod $endingEliminationPeriod): self
	{
		// unset the owning side of the relation if necessary
		if ($endingEliminationPeriod === null && $this->endingEliminationPeriod !== null) {
			$this->endingEliminationPeriod->setEndVisit(null);
		}

		// set the owning side of the relation if necessary
		if ($endingEliminationPeriod !== null && $endingEliminationPeriod->getEndVisit() !== $this) {
			$endingEliminationPeriod->setEndVisit($this);
		}

		$this->endingEliminationPeriod = $endingEliminationPeriod;

		return $this;
	}

	public function getDiverseObservation(): ?string
	{
		return $this->diverseObservation;
	}

	public function setDiverseObservation(?string $diverseObservation): self
	{
		$this->diverseObservation = $diverseObservation;

		return $this;
	}

	public function getInternObservation(): ?string
	{
		return $this->internObservation;
	}

	public function setInternObservation(?string $internObservation): self
	{
		$this->internObservation = $internObservation;

		return $this;
	}

	public function getClientReportFileID(): ?string
	{
		return $this->clientReportFileID;
	}

	public function setClientReportFileID(?string $clientReportFileID): self
	{
		$this->clientReportFileID = $clientReportFileID;

		return $this;
	}

	public function getInternReportFileID(): ?string
	{
		return $this->InternReportFileID;
	}

	public function setInternReportFileID(?string $InternReportFileID): self
	{
		$this->InternReportFileID = $InternReportFileID;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getDurationAdjustments(): array
	{
		return $this->durationAdjustments;
	}

	/**
	 * @param array $durationAdjustments
	 * @return Visit
	 */
	public function setDurationAdjustments(array $durationAdjustments): Visit
	{
		$this->durationAdjustments = $durationAdjustments;
		return $this;
	}

	/**
	 * @return Collection|VisitStepTypeAdjustment[]
	 */
	public function getVisitStepTypeAdjustments(): Collection
	{
		return $this->visitStepTypeAdjustments;
	}

	public function addVisitStepTypeAdjustment(?VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if ($visitStepTypeAdjustment && !$this->visitStepTypeAdjustments->contains($visitStepTypeAdjustment)) {
			$this->visitStepTypeAdjustments[] = $visitStepTypeAdjustment;
			$visitStepTypeAdjustment->setAddedDuringVisit($this);
		}

		return $this;
	}

	public function removeVisitStepTypeAdjustment(VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if ($this->visitStepTypeAdjustments->removeElement($visitStepTypeAdjustment)) {
			// set the owning side to null (unless already changed)
			if ($visitStepTypeAdjustment->getAddedDuringVisit() === $this) {
				$visitStepTypeAdjustment->setAddedDuringVisit(null);
			}
		}

		return $this;
	}

	public function getNextScheduledVisit(): ?NextScheduledVisit
	{
		return $this->nextScheduledVisit;
	}

	public function setNextScheduledVisit(NextScheduledVisit $nextScheduledVisit): self
	{
		// set the owning side of the relation if necessary
		if ($nextScheduledVisit->getPreviousVisit() !== $this) {
			$nextScheduledVisit->setPreviousVisit($this);
		}

		$this->nextScheduledVisit = $nextScheduledVisit;

		return $this;
	}

	public function getIsDraft(): ?bool
	{
		return $this->isDraft;
	}

	public function setIsDraft(bool $isDraft): self
	{
		$this->isDraft = $isDraft;

		return $this;
	}

}

