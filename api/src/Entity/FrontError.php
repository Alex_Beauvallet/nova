<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\FrontErrorRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource]
#[ORM\Entity(repositoryClass: FrontErrorRepository::class)]
class FrontError implements UserOwnedInterface
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;


	#[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'frontErrors')]
	#[ORM\JoinColumn(nullable: false)]
	private ?User $createdBy;

	#[ORM\Column(type: 'datetime_immutable')]
	private DateTimeImmutable $createdAt;


	#[ORM\Column(type: 'string', length: 255)]
	private ?string $errorName;

	#[ORM\Column(type: 'text')]
	private ?string $ErrorStackTrace;

	#[ORM\Column(type: 'json')]
	private $localStorage = [];

	public function __construct()
	{
		$this->createdAt = new DateTimeImmutable("now");
	}


	public function getId(): ?int
	{
		return $this->id;
	}

	public function getCreatedBy(): ?User
	{
		return $this->createdBy;
	}

	public function setCreatedBy(?User $createdBy): self
	{
		$this->createdBy = $createdBy;

		return $this;
	}

	/**
	 * @return DateTimeImmutable
	 */
	public function getCreatedAt(): DateTimeImmutable
	{
		return $this->createdAt;
	}


	public function getErrorName(): ?string
	{
		return $this->errorName;
	}

	public function setErrorName(string $errorName): self
	{
		$this->errorName = $errorName;

		return $this;
	}

	public function getErrorStackTrace(): ?string
	{
		return $this->ErrorStackTrace;
	}

	public function setErrorStackTrace(string $ErrorStackTrace): self
	{
		$this->ErrorStackTrace = $ErrorStackTrace;

		return $this;
	}

	public function getLocalStorage(): ?array
	{
		return $this->localStorage;
	}

	public function setLocalStorage(array $localStorage): self
	{
		$this->localStorage = $localStorage;

		return $this;
	}
}
