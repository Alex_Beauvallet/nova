<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Filter\VisitTypeFilter;
use App\Repository\VisitTypeRepository;
use App\State\VisitTypeProvider;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: VisitTypeRepository::class)]
#[ApiResource(
		normalizationContext: ['groups' => ['worksite', 'visit_type']],
		security: 'is_granted("ROLE_USER")',
		provider: VisitTypeProvider::class
)]
#[ApiFilter(SearchFilter::class, properties: ['worksite_status.short_name' => 'exact'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_allowed_from_installation_only'])]
#[ApiFilter(VisitTypeFilter::class)]
class VisitType
{
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	#[ORM\ManyToMany(targetEntity: VisitStepType::class)]
	public $steps;

	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;
	#[ORM\OneToMany(mappedBy: 'next_visit_type', targetEntity: Worksite::class)]
	private $worksites;

	#[ORM\OneToMany(mappedBy: 'type', targetEntity: Visit::class)]
	private $visits;

	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	#[ORM\Column(type: 'string', length: 255)]
	private $short_name;

	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	#[ORM\Column(type: 'string', length: 255)]
	private $long_name;

	#[Groups(['visit_type'])]
	#[ORM\Column(type: 'dateinterval', nullable: true)]
	private $delay_between_visits;

	#[Groups(['visit_type'])]
	#[ORM\ManyToOne(targetEntity: WorksiteStatus::class, inversedBy: 'visitTypes')]
	#[ORM\JoinColumn(nullable: true)]
	private $worksite_status;

	#[Groups(['visit_type'])]
	#[ORM\Column(type: 'boolean')]
	private $is_allowed_from_installation_only;

	#[Groups(['visit_type'])]
	#[ORM\Column(type: 'boolean')]
	private $has_fixed_planification;

	private ?int $worksite_id;

	#[Groups(['visit_type'])]
	#[ORM\Column(type: 'dateinterval')]
	private $relational_time;

	public function __construct()
	{
		$this->steps = new ArrayCollection();
		$this->worksites = new ArrayCollection();
		$this->visits = new ArrayCollection();
	}

	#[Groups('visit_type')]
	public function getId(): ?int
	{
		return $this->id;
	}

	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	public function getCode(): ?string
	{
		return implode(" + ", $this->steps->map(fn($s) => $s->getCode())->toArray());
	}

	/**
	 * @return Collection|VisitStepType[]
	 */
	public function getSteps(): Collection
	{
		return $this->steps;
	}

	public function addStep(VisitStepType $step): self
	{
		if (!$this->steps->contains($step)) {
			$this->steps[] = $step;
		}

		return $this;
	}

	public function removeStep(VisitStepType $step): self
	{
		$this->steps->removeElement($step);

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->setNextVisitType($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			// set the owning side to null (unless already changed)
			if ($worksite->getNextVisitType() === $this) {
				$worksite->setNextVisitType(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Visit[]
	 */
	public function getVisits(): Collection
	{
		return $this->visits;
	}

	public function addVisit(Visit $visit): self
	{
		if (!$this->visits->contains($visit)) {
			$this->visits[] = $visit;
			$visit->setType($this);
		}

		return $this;
	}

	public function removeVisit(Visit $visit): self
	{
		if ($this->visits->removeElement($visit)) {
			// set the owning side to null (unless already changed)
			if ($visit->getType() === $this) {
				$visit->setType(null);
			}
		}

		return $this;
	}

	public function getShortName(): ?string
	{
		return $this->short_name;
	}

	public function setShortName(string $short_name): self
	{
		$this->short_name = $short_name;

		return $this;
	}

	public function getLongName(): ?string
	{
		return $this->long_name;
	}

	public function setLongName(string $long_name): self
	{
		$this->long_name = $long_name;

		return $this;
	}

	public function getDelayBetweenVisits(): ?DateInterval
	{
		return $this->delay_between_visits;
	}

	public function setDelayBetweenVisits(?DateInterval $delay_between_visits): self
	{
		$this->delay_between_visits = $delay_between_visits;

		return $this;
	}

	public function getWorksiteStatus(): ?WorksiteStatus
	{
		return $this->worksite_status;
	}

	public function setWorksiteStatus(?WorksiteStatus $worksite_status): self
	{
		$this->worksite_status = $worksite_status;

		return $this;
	}

	public function getIsAllowedFromInstallationOnly(): ?bool
	{
		return $this->is_allowed_from_installation_only;
	}

	public function setIsAllowedFromInstallationOnly(bool $is_allowed_from_installation_only): self
	{
		$this->is_allowed_from_installation_only = $is_allowed_from_installation_only;

		return $this;
	}

	public function getHasFixedPlanification(): ?bool
	{
		return $this->has_fixed_planification;
	}

	public function setHasFixedPlanification(bool $has_fixed_planification): self
	{
		$this->has_fixed_planification = $has_fixed_planification;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getWorksiteId(): ?int
	{
		return $this->worksite_id;
	}

	/**
	 * @param int|null $worksite_id
	 * @return VisitType
	 */
	public function setWorksiteId(?int $worksite_id): VisitType
	{
		$this->worksite_id = $worksite_id;
		return $this;
	}

	public function getRelationalTime(): ?DateInterval
	{
		return $this->relational_time;
	}

	public function setRelationalTime(DateInterval $relational_time): self
	{
		$this->relational_time = $relational_time;

		return $this;
	}
}
