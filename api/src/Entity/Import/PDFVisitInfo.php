<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity\Import;


use DateInterval;

class PDFVisitInfo
{

	private bool $isEliminationBegin = false;
	private bool $isEliminationEnd = false;
	private string $diverseObservations = "";
	private string $internObservations = "";
	private ?DateInterval $effectiveDuration;

	public function __construct()
	{
		$this->effectiveDuration = DateInterval::createFromDateString('0 min');
	}

	/**
	 * @return DateInterval|null
	 */
	public function getEffectiveDuration(): ?DateInterval
	{
		return $this->effectiveDuration;
	}

	/**
	 * @param DateInterval|null $effectiveDuration
	 * @return PDFVisitInfo
	 */
	public function setEffectiveDuration(?DateInterval $effectiveDuration): PDFVisitInfo
	{
		$this->effectiveDuration = $effectiveDuration;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isEliminationBegin(): bool
	{
		return $this->isEliminationBegin;
	}

	/**
	 * @param bool $isEliminationBegin
	 * @return PDFVisitInfo
	 */
	public function setIsEliminationBegin(bool $isEliminationBegin): PDFVisitInfo
	{
		$this->isEliminationBegin = $isEliminationBegin;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isEliminationEnd(): bool
	{
		return $this->isEliminationEnd;
	}

	/**
	 * @param bool $isEliminationEnd
	 * @return PDFVisitInfo
	 */
	public function setIsEliminationEnd(bool $isEliminationEnd): PDFVisitInfo
	{
		$this->isEliminationEnd = $isEliminationEnd;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDiverseObservations(): string
	{
		return $this->diverseObservations;
	}

	/**
	 * @param string $diverseObservations
	 * @return PDFVisitInfo
	 */
	public function setDiverseObservations(string $diverseObservations): PDFVisitInfo
	{
		$this->diverseObservations = $diverseObservations;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getInternObservations(): string
	{
		return $this->internObservations;
	}

	/**
	 * @param string $internObservations
	 * @return PDFVisitInfo
	 */
	public function setInternObservations(string $internObservations): PDFVisitInfo
	{
		$this->internObservations = $internObservations;
		return $this;
	}

}
