<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity\Import;


use App\Entity\StationStatus;
use App\Entity\StationType;
use App\Entity\Visit;

class PDFStation
{

	private int $id;

	private int $number;

	private StationType $type;

	private StationStatus $stationStatus;

	private ?Visit $installedDuringVisit = null;

	/**
	 * @return Visit|null
	 */
	public function getInstalledDuringVisit(): ?Visit
	{
		return $this->installedDuringVisit;
	}

	/**
	 * @param Visit|null $installedDuringVisit
	 * @return PDFStation
	 */
	public function setInstalledDuringVisit(?Visit $installedDuringVisit): PDFStation
	{
		$this->installedDuringVisit = $installedDuringVisit;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return PDFStation
	 */
	public function setId(int $id): PDFStation
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getNumber(): int
	{
		return $this->number;
	}

	/**
	 * @param int $number
	 * @return PDFStation
	 */
	public function setNumber(int $number): PDFStation
	{
		$this->number = $number;
		return $this;
	}

	/**
	 * @return StationStatus
	 */
	public function getStationStatus(): StationStatus
	{
		return $this->stationStatus;
	}

	/**
	 * @param StationStatus $stationStatus
	 * @return PDFStation
	 */
	public function setStationStatus(StationStatus $stationStatus): PDFStation
	{
		$this->stationStatus = $stationStatus;
		return $this;
	}

	/**
	 * @return StationType
	 */
	public function getType(): StationType
	{
		return $this->type;
	}

	/**
	 * @param StationType $type
	 * @return PDFStation
	 */
	public function setType(StationType $type): self
	{
		$this->type = $type;
		return $this;
	}

}
