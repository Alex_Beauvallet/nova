<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\PredefinedObservationsTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ['groups' => ['predefined_observations']])]
#[ORM\Entity(repositoryClass: PredefinedObservationsTypeRepository::class)]
class PredefinedObservationsType
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups('predefined_observations')]
	#[ORM\Column(type: 'string', length: 255)]
	private $type;

	#[ORM\OneToMany(mappedBy: 'type', targetEntity: PredefinedObservations::class)]
	private $predefinedObservations;

	public function __construct()
	{
		$this->predefinedObservations = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @return Collection|PredefinedObservations[]
	 */
	public function getPredefinedObservations(): Collection
	{
		return $this->predefinedObservations;
	}

	public function addPredefinedObservation(PredefinedObservations $predefinedObservation): self
	{
		if (!$this->predefinedObservations->contains($predefinedObservation)) {
			$this->predefinedObservations[] = $predefinedObservation;
			$predefinedObservation->setType($this);
		}

		return $this;
	}

	public function removePredefinedObservation(PredefinedObservations $predefinedObservation): self
	{
		if ($this->predefinedObservations->removeElement($predefinedObservation)) {
			// set the owning side to null (unless already changed)
			if ($predefinedObservation->getType() === $this) {
				$predefinedObservation->setType(null);
			}
		}

		return $this;
	}

	public function getType(): ?string
	{
		return $this->type;
	}

	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}
}
