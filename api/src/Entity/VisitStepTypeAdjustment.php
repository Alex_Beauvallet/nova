<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\VisitStepTypeAdjustmentRepository;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ['groups' => ['visit_type']])]
#[ORM\Entity(repositoryClass: VisitStepTypeAdjustmentRepository::class)]
class VisitStepTypeAdjustment
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[ORM\ManyToOne(targetEntity: VisitStepType::class, inversedBy: 'adjustments')]
	#[ORM\JoinColumn(nullable: false)]
	private $visitStepType;

	#[ORM\ManyToOne(targetEntity: Worksite::class, inversedBy: 'visitStepTypeAdjustments')]
	#[ORM\JoinColumn(nullable: false)]
	private $worksite;

	#[ORM\Column(type: 'datetime_immutable')]
	private $created_at;

	#[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'visitStepTypeAdjustments')]
	#[ORM\JoinColumn(nullable: false)]
	private $created_by;

	#[Groups(['visit_type'])]
	#[ORM\Column(type: 'dateinterval')]
	private $duration;

	#[ORM\ManyToOne(targetEntity: Visit::class, inversedBy: 'visitStepTypeAdjustments')]
	#[ORM\JoinColumn(nullable: false)]
	private $addedDuringVisit;

	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return VisitStepTypeAdjustment
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	public function getVisitStepType(): ?VisitStepType
	{
		return $this->visitStepType;
	}

	public function setVisitStepType(?VisitStepType $visitStepType): self
	{
		$this->visitStepType = $visitStepType;

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeImmutable
	{
		return $this->created_at;
	}

	public function setCreatedAt(DateTimeImmutable $created_at): self
	{
		$this->created_at = $created_at;

		return $this;
	}

	public function getCreatedBy(): ?User
	{
		return $this->created_by;
	}

	public function setCreatedBy(?User $created_by): self
	{
		$this->created_by = $created_by;

		return $this;
	}

	public function getDuration(): ?DateInterval
	{
		return $this->duration;
	}

	public function setDuration(DateInterval $duration): self
	{
		$this->duration = $duration;

		return $this;
	}

	public function getIsDraft(): bool
	{
		return $this->getAddedDuringVisit()->getIsDraft();
	}

	public function getAddedDuringVisit(): ?Visit
	{
		return $this->addedDuringVisit;
	}

	public function setAddedDuringVisit(?Visit $addedDuringVisit): self
	{
		$this->addedDuringVisit = $addedDuringVisit;

		return $this;
	}
}
