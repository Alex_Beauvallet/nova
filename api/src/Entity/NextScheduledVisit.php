<?php

namespace App\Entity;

use App\Repository\NextScheduledVisitRepository;
use DateInterval;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: NextScheduledVisitRepository::class)]
class NextScheduledVisit
{
	#[Groups('write:visit')]
	#[ORM\Column(type: 'datetime')]
	private DateTimeInterface $scheduledAt;

	#[Groups('write:visit')]
	#[ORM\Column(type: 'dateinterval')]
	private DateInterval $estimatedDuration;

	#[Groups('write:visit')]
	#[ORM\ManyToOne(targetEntity: Worksite::class)]
	#[ORM\JoinColumn(nullable: false)]
	private ?Worksite $worksite;

	#[Groups('write:visit')]
	#[ORM\ManyToOne(targetEntity: VisitType::class)]
	#[ORM\JoinColumn(nullable: false)]
	private ?VisitType $type;

	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[ORM\OneToOne(inversedBy: 'nextScheduledVisit', targetEntity: Visit::class, cascade: ['persist', 'remove'])]
	#[ORM\JoinColumn(nullable: false)]
	private ?Visit $previousVisit = null;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return NextScheduledVisit
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return DateTimeInterface
	 */
	public function getScheduledAt(): DateTimeInterface
	{
		return $this->scheduledAt;
	}

	/**
	 * @param DateTimeInterface $scheduledAt
	 * @return NextScheduledVisit
	 */
	public function setScheduledAt(DateTimeInterface $scheduledAt): NextScheduledVisit
	{
		$this->scheduledAt = $scheduledAt;
		return $this;
	}

	/**
	 * @return DateInterval
	 */
	public function getEstimatedDuration(): DateInterval
	{
		return $this->estimatedDuration;
	}

	/**
	 * @param DateInterval $estimatedDuration
	 * @return NextScheduledVisit
	 */
	public function setEstimatedDuration(DateInterval $estimatedDuration): NextScheduledVisit
	{
		$this->estimatedDuration = $estimatedDuration;
		return $this;
	}

	/**
	 * @return Worksite|null
	 */
	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	/**
	 * @param Worksite|null $worksite
	 * @return NextScheduledVisit
	 */
	public function setWorksite(?Worksite $worksite): NextScheduledVisit
	{
		$this->worksite = $worksite;
		return $this;
	}

	/**
	 * @return VisitType|null
	 */
	public function getType(): ?VisitType
	{
		return $this->type;
	}

	/**
	 * @param VisitType|null $type
	 * @return NextScheduledVisit
	 */
	public function setType(?VisitType $type): NextScheduledVisit
	{
		$this->type = $type;
		return $this;
	}

	public function getPreviousVisit(): ?Visit
	{
		return $this->previousVisit;
	}

	public function setPreviousVisit(Visit $previousVisit): self
	{
		$this->previousVisit = $previousVisit;

		return $this;
	}
}
