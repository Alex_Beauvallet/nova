<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;


use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use App\Controller\MeController;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
	operations: [
		new Get(
			uriTemplate: '/me',
			controller: MeController::class,
			read: false
		)
	],
	normalizationContext: ['groups' => ['read:User']],
	security: 'is_granted("ROLE_USER")'
)]

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table("`user`")]
class User implements UserInterface, PasswordAuthenticatedUserInterface, JWTUserInterface
{
	#[Groups(['read:User'])]
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private ?int $id;

	#[Groups(['read:User'])]
	#[ORM\Column(type: 'string', length: 180, unique: true)]
	private ?string $email;

	#[Groups(['read:User'])]
	#[ORM\Column(type: 'json')]
	private array $roles = [];

	/**
	 * @var string The hashed password
	 */
	#[ORM\Column(type: 'string')]
	private string $password = "";

	#[Groups(['read:User'])]
	#[ORM\Column(type: 'string', length: 255)]
	private ?string $first_name = '';

	#[Groups(['read:User'])]
	#[ORM\Column(type: 'string', length: 255)]
	private ?string $last_name = '';

	#[Groups(['read:User'])]
	#[ORM\Column(type: 'string', length: 255, nullable: true)]
	private $calendar_id;

	#[ORM\OneToMany(mappedBy: 'created_by', targetEntity: VisitStepTypeAdjustment::class, orphanRemoval: true)]
	private $visitStepTypeAdjustments;

	#[ORM\OneToMany(mappedBy: 'createdBy', targetEntity: FrontError::class)]
	private $frontErrors;

	public function __construct()
	{
		$this->visitStepTypeAdjustments = new ArrayCollection();
		$this->frontErrors = new ArrayCollection();
	}

	public static function createFromPayload($username, array $payload): User
	{
		$user = new User();
		$user->setId(intval($username))->setEmail($payload["email"])->setCalendarId($payload["calendar_id"]);
		return $user;
	}

	public function getId(): ?string
	{
		return strval($this->id);
	}

	/**
	 * @param int? $id
	 * @return User
	 */
	public function setId($id): self
	{
		$this->id = $id;
		return $this;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * A visual identifier that represents this user.
	 *
	 * @see UserInterface
	 */
	public function getUserIdentifier(): string
	{
		return (string)$this->email;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = $this->roles;
		// guarantee every user at least has ROLE_USER
		$roles[] = 'ROLE_USER';

		return array_unique($roles);
	}

	public function setRoles(array $roles): self
	{
		$this->roles = $roles;

		return $this;
	}

	/**
	 * @see PasswordAuthenticatedUserInterface
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(string $password): self
	{
		$this->password = $password;

		return $this;
	}

	/**
	 * Returning a salt is only needed, if you are not using a modern
	 * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
	 *
	 * @see UserInterface
	 */
	public function getSalt(): ?string
	{
		return null;
	}

	/**
	 * @see UserInterface
	 */
	public function eraseCredentials()
	{
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}

	public function getUsername(): string
	{
		return (string)$this->email;
	}

	public function getFirstName(): ?string
	{
		return $this->first_name;
	}

	public function setFirstName(string $first_name): self
	{
		$this->first_name = $first_name;

		return $this;
	}

	public function getLastName(): ?string
	{
		return $this->last_name;
	}

	public function setLastName(string $last_name): self
	{
		$this->last_name = $last_name;

		return $this;
	}

	public function getCalendarId(): ?string
	{
		return $this->calendar_id;
	}

	public function setCalendarId(?string $calendar_id): self
	{
		$this->calendar_id = $calendar_id;

		return $this;
	}

	/**
	 * @return Collection|VisitStepTypeAdjustment[]
	 */
	public function getVisitStepTypeAdjustments(): Collection
	{
		return $this->visitStepTypeAdjustments;
	}

	public function addVisitStepTypeAdjustment(VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if (!$this->visitStepTypeAdjustments->contains($visitStepTypeAdjustment)) {
			$this->visitStepTypeAdjustments[] = $visitStepTypeAdjustment;
			$visitStepTypeAdjustment->setCreatedBy($this);
		}

		return $this;
	}

	public function removeVisitStepTypeAdjustment(VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if ($this->visitStepTypeAdjustments->removeElement($visitStepTypeAdjustment)) {
			// set the owning side to null (unless already changed)
			if ($visitStepTypeAdjustment->getCreatedBy() === $this) {
				$visitStepTypeAdjustment->setCreatedBy(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection<int, FrontError>
	 */
	public function getFrontErrors(): Collection
	{
		return $this->frontErrors;
	}

	public function addFrontError(FrontError $frontError): self
	{
		if (!$this->frontErrors->contains($frontError)) {
			$this->frontErrors[] = $frontError;
			$frontError->setCreatedBY($this);
		}

		return $this;
	}

	public function removeFrontError(FrontError $frontError): self
	{
		if ($this->frontErrors->removeElement($frontError)) {
			// set the owning side to null (unless already changed)
			if ($frontError->getCreatedBY() === $this) {
				$frontError->setCreatedBY(null);
			}
		}

		return $this;
	}
}
