<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\WorksiteStatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(normalizationContext: ['groups' => ['worksite']])]
#[ORM\Entity(repositoryClass: WorksiteStatusRepository::class)]
class WorksiteStatus
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups(['worksite', 'worksite_summary'])]
	#[ORM\Column(type: 'string', length: 255)]
	private $short_name;

	#[ORM\Column(type: 'string', length: 255)]
	private $long_name;

	#[ORM\OneToMany(mappedBy: 'status', targetEntity: Worksite::class)]
	private $worksites;

	#[ORM\OneToMany(mappedBy: 'worksite_status', targetEntity: VisitType::class)]
	private $visitTypes;

	public function __construct()
	{
		$this->worksites = new ArrayCollection();
		$this->visitTypes = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getShortName(): ?string
	{
		return $this->short_name;
	}

	public function setShortName(string $short_name): self
	{
		$this->short_name = $short_name;

		return $this;
	}

	public function getLongName(): ?string
	{
		return $this->long_name;
	}

	public function setLongName(string $long_name): self
	{
		$this->long_name = $long_name;

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->setStatus($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			// set the owning side to null (unless already changed)
			if ($worksite->getStatus() === $this) {
				$worksite->setStatus(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|VisitType[]
	 */
	public function getVisitTypes(): Collection
	{
		return $this->visitTypes;
	}

	public function addVisitType(VisitType $visitType): self
	{
		if (!$this->visitTypes->contains($visitType)) {
			$this->visitTypes[] = $visitType;
			$visitType->setWorksiteStatus($this);
		}

		return $this;
	}

	public function removeVisitType(VisitType $visitType): self
	{
		if ($this->visitTypes->removeElement($visitType)) {
			// set the owning side to null (unless already changed)
			if ($visitType->getWorksiteStatus() === $this) {
				$visitType->setWorksiteStatus(null);
			}
		}

		return $this;
	}
}

