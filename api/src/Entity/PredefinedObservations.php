<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\PredefinedObservationsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
		normalizationContext: ['groups' => ['predefined_observations']],
		security: 'is_granted("ROLE_USER")'
	)]
#[ORM\Entity(repositoryClass: PredefinedObservationsRepository::class)]
class PredefinedObservations
{
	#[ORM\Id]
	#[ORM\GeneratedValue]
	#[ORM\Column(type: 'integer')]
	private $id;

	#[Groups('predefined_observations')]
	#[ORM\Column(type: 'integer')]
	private $number;

	#[Groups('predefined_observations')]
	#[ORM\Column(type: 'text')]
	private $text;

	#[Groups('predefined_observations')]
	#[ORM\ManyToOne(targetEntity: PredefinedObservationsType::class, inversedBy: 'predefinedObservations')]
	#[ORM\JoinColumn(nullable: false)]
	private $type;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getNumber(): ?int
	{
		return $this->number;
	}

	public function setNumber(int $number): self
	{
		$this->number = $number;

		return $this;
	}

	public function getText(): ?string
	{
		return $this->text;
	}

	public function setText(string $text): self
	{
		$this->text = $text;

		return $this;
	}

	public function getType(): ?PredefinedObservationsType
	{
		return $this->type;
	}

	public function setType(?PredefinedObservationsType $type): self
	{
		$this->type = $type;

		return $this;
	}
}
