<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\State;

use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Worksite;
use App\Service\TypesenseService;
use Http\Client\Exception;
use Typesense\Exceptions\TypesenseClientError;

readonly class WorksiteProcessor implements ProcessorInterface
{

	public function __construct(private ProcessorInterface $persistProcessor, private ProcessorInterface $removeProcessor, private TypesenseService $typesenseService)
	{
	}

	public function supports($data, array $context = []): bool
	{
		return $data instanceof Worksite && $this->persistProcessor->supports($data, $context);
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
	{
		if ($operation instanceof DeleteOperationInterface) {
			$this->removeProcessor->process($data, $operation, $uriVariables, $context);
			$this->typesenseService->deleteWorksite($uriVariables['id']);
		}

		$this->persistProcessor->process($data, $operation, $uriVariables, $context);
		$this->typesenseService->upsertWorksite($data);
	}
}
