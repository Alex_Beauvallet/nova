<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\State;


use ApiPlatform\Metadata\DeleteOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Station;
use App\Entity\StationStatus;
use App\Entity\User;
use App\Entity\Visit;
use App\Entity\VisitStepType;
use App\Entity\VisitStepTypeAdjustment;
use App\Entity\Worksite;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\SecurityBundle\Security;

readonly class VisitProcessor implements ProcessorInterface
{

	public function __construct(private ProcessorInterface $persistProcessor, private ProcessorInterface $removeProcessor, private Security $security, private EntityManagerInterface $entityManager)
	{
	}

	/**
	 * @param mixed $data
	 * @param Operation $operation
	 * @param array $uriVariables
	 * @param array $context
	 * @return mixed
	 * @throws Exception
	 * Persists data in database
	 */
	public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): mixed
	{
		if ($operation instanceof DeleteOperationInterface) {
			return $this->removeProcessor->process($data, $operation, $uriVariables, $context);
		}

		if ($data instanceof Visit) {

			//Creating visit duration adjustment
			array_map(
				fn($minutesDuration, $stepType) => $data->addVisitStepTypeAdjustment($this->createDurationAdjustments($stepType, $minutesDuration, $data->getWorksite())),
				$data->getDurationAdjustments(),
				array_keys($data->getDurationAdjustments()));
			//Creating new stations
			$data->getStationStatuses()
				->filter(fn(StationStatus $s) => $s->getStation() === null)
				->map(fn(StationStatus $s) => $this->createStation($s, $data->getWorksite(), $data));
			// Suspending or removing stations
			$data->getStationStatuses()->map(function (StationStatus $s) {
				if (($station = $s->getStation()) instanceof Station) {
					$station->setIsSuspended($s->getIsSuspended());
					if ($s->getIsRemoved() && !$s->getIsSuspended()) {
						$station->setIsInstalled(false);
					}
				}
			});

			// Setting next scheduled visit on worksite entity
			$nextVisit = $data->getNextScheduledVisit()->setWorksite($data->getWorksite())->setId(0);
			$data->getWorksite()->setNextVisitType($nextVisit->getType())
				->setNextVisitLimitDate($nextVisit->getScheduledAt());
			return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
		}
		throw new Exception("L'objet fourni n'est pas une entité Visite");
	}

	/**
	 * Creates duration adjustments for a specific step type and worksite
	 * @param string $stepType Step type
	 * @param int $minutesDuration Adjustment duration
	 * @param Worksite $worksite Worksite
	 * @return VisitStepTypeAdjustment|null
	 */
	private function createDurationAdjustments(string $stepType, int $minutesDuration, Worksite $worksite): ?VisitStepTypeAdjustment
	{
		if ($minutesDuration !== 0) {
			$negative = $minutesDuration < 0;
			$type = $this->entityManager->getRepository(VisitStepType::class)->findOneBy(["short_name" => $stepType]);
			$user = $this->security->getUser();
			if ($user instanceof User && $type instanceof VisitStepType) {
				$minutesDuration = abs($minutesDuration);
				$interval = DateInterval::createFromDateString("$minutesDuration min");
				if ($negative) {
					$interval->invert = 1;
				}
				return (new VisitStepTypeAdjustment())
					->setWorksite($worksite)
					->setDuration($interval)
					->setCreatedAt(new DateTimeImmutable())
					->setCreatedBy($user)
					->setVisitStepType($type);
			}
		}
		return null;
	}

	/**
	 * Creates a station corresponding to a stationStatus and adds it to the stationStatus
	 * @param StationStatus $status Original stationStatus object
	 * @param Worksite $worksite Concerned worksite
	 * @param Visit $visit Concerned visit
	 * @return StationStatus
	 */
	private function createStation(StationStatus $status, Worksite $worksite, Visit $visit): StationStatus
	{
		$station = (new Station())
			->setWorksite($worksite)
			->setInstalledDuringVisit($visit)
			->setNumber($status->getStationNumber())
			->setIsSuspended($status->getIsSuspended())
			->setIsInstalled(true)
			->setMasterStation($status->getMasterStation())
			->setIndexName($status->getIndexName())
			->setType($status->getStationType());
		return $status->setStation($station);
	}
}
