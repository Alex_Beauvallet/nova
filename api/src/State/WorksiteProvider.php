<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\State;


use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Worksite;
use App\Service\ScheduledVisitService;
use DateInterval;
use Exception;

readonly class WorksiteProvider implements ProviderInterface
{

	public function __construct(private ProviderInterface $collectionProvider, private ScheduledVisitService $scheduledVisitService)
	{
	}

	/**
	 * @throws Exception
	 */
	public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
	{
		$collection = $this->collectionProvider->provide($operation, $uriVariables, $context);

		foreach ($collection as $worksite) {
			/**
			 * @var $worksite Worksite
			 */
			$visits = $this->scheduledVisitService->getNextScheduledVisits($worksite);
			$worksite->setNextScheduledVisits($visits);
			if (count($visits) === 0) {
				$visitType = $this->scheduledVisitService->getTypeWithCalculatedStepDuration($worksite->getNextVisitType(), $worksite, DateInterval::createFromDateString("0 min"));
				$worksite->setNextVisitType($visitType);
			}
		}
		return $collection;
	}
}

