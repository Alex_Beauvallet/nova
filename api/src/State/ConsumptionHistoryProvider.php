<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Repository\WorksiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

readonly class ConsumptionHistoryProvider implements ProviderInterface
{

	public function __construct(private EntityManagerInterface $entityManager)
	{
	}

	/**
	 * @throws NonUniqueResultException
	 */
	public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
	{
		$id = $uriVariables['id'];
		$resourceClass = $context["resource_class"];
		$repository = $this->entityManager->getRepository($resourceClass);
		if ($repository instanceof WorksiteRepository) {
			return $repository->findByIdWithOrderedVisits($id);
		}
		return null;
	}
}
