<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

namespace App\State;


use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\NextScheduledVisit;
use App\Entity\Visit;
use App\Service\GoogleCalendarService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

readonly class VisitValidationProcessor implements ProcessorInterface
{

	public function __construct(private ProcessorInterface $persistProcessor, private GoogleCalendarService $calendarService, private EntityManagerInterface $entityManager)
	{
	}

	/**
	 * @param mixed $data
	 * @param Operation $operation
	 * @param array $uriVariables
	 * @param array $context
	 * @return void
	 * Persists data and creates a Google Calendar event only if the visit has not already been validated
	 * @throws Exception
	 */
	public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
	{
		if ($data instanceof Visit) {
			if ($data->getIsCompleted() && !$data->getIsDraft()) {
				$nextVisit = $this->entityManager->getRepository(NextScheduledVisit::class)->findOneBy(["previousVisit" => $data->getId()]);
				if ($nextVisit instanceof NextScheduledVisit) {
					$this->calendarService->createEvent($nextVisit);
					$this->persistProcessor->process($data, $operation, $uriVariables, $context);
					return;
				}
			} else {
				throw new HttpException(Response::HTTP_FORBIDDEN, "Une visite validée ou non terminée ne peut pas changer de statut");
			}

		}
		throw new Exception("L'objet fourni n'est pas une entité Visite");
	}
}
