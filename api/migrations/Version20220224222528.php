<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220224222528 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE next_scheduled_visit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE next_scheduled_visit (id INT NOT NULL, worksite_id INT NOT NULL, type_id INT NOT NULL, previous_visit_id INT NOT NULL, scheduled_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, scheduled_duration VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_35C9F822A47737E7 ON next_scheduled_visit (worksite_id)');
        $this->addSql('CREATE INDEX IDX_35C9F822C54C8C93 ON next_scheduled_visit (type_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_35C9F8226A1117F6 ON next_scheduled_visit (previous_visit_id)');
        $this->addSql('COMMENT ON COLUMN next_scheduled_visit.scheduled_duration IS \'(DC2Type:dateinterval)\'');
        $this->addSql('ALTER TABLE next_scheduled_visit ADD CONSTRAINT FK_35C9F822A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE next_scheduled_visit ADD CONSTRAINT FK_35C9F822C54C8C93 FOREIGN KEY (type_id) REFERENCES visit_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE next_scheduled_visit ADD CONSTRAINT FK_35C9F8226A1117F6 FOREIGN KEY (previous_visit_id) REFERENCES visit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE next_scheduled_visit_id_seq CASCADE');
        $this->addSql('DROP TABLE next_scheduled_visit');
    }
}
