<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210808192944 extends AbstractMigration
{
	public function getDescription(): string
	{
		return '';
	}

	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('ALTER TABLE visit_type ADD worksite_status_id INT DEFAULT NULL');
		$this->addSql('ALTER TABLE visit_type ADD delay_between_visits INT DEFAULT NULL');
		$this->addSql('ALTER TABLE visit_type ADD CONSTRAINT FK_5386AAEBE2704FD2 FOREIGN KEY (worksite_status_id) REFERENCES worksite_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('CREATE INDEX IDX_5386AAEBE2704FD2 ON visit_type (worksite_status_id)');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SCHEMA public');
		$this->addSql('ALTER TABLE visit_type DROP CONSTRAINT FK_5386AAEBE2704FD2');
		$this->addSql('DROP INDEX IDX_5386AAEBE2704FD2');
		$this->addSql('ALTER TABLE visit_type DROP worksite_status_id');
		$this->addSql('ALTER TABLE visit_type DROP delay_between_visits');
	}
}
