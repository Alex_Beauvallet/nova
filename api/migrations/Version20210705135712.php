<?php
/*
 * Copyright 2021,2022 Maxime Girardet
 *
 * This file is part of Nova.
 *
 * Nova is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Nova is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with Nova. If not, see <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210705135712 extends AbstractMigration
{
	public function getDescription(): string
	{
		return '';
	}

	public function up(Schema $schema): void
	{
		// this up() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SEQUENCE contacts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
		$this->addSql('CREATE TABLE contacts (id INT NOT NULL, name VARCHAR(255) NOT NULL, phone_number VARCHAR(10) NOT NULL, email_address VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
		$this->addSql('CREATE UNIQUE INDEX UNIQ_334015736B01BC5B ON contacts (phone_number)');
		$this->addSql('CREATE UNIQUE INDEX UNIQ_33401573B08E074E ON contacts (email_address)');
		$this->addSql('CREATE TABLE worksite_contacts (worksite_id INT NOT NULL, contacts_id INT NOT NULL, PRIMARY KEY(worksite_id, contacts_id))');
		$this->addSql('CREATE INDEX IDX_67249AEAA47737E7 ON worksite_contacts (worksite_id)');
		$this->addSql('CREATE INDEX IDX_67249AEA719FB48E ON worksite_contacts (contacts_id)');
		$this->addSql('ALTER TABLE worksite_contacts ADD CONSTRAINT FK_67249AEAA47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
		$this->addSql('ALTER TABLE worksite_contacts ADD CONSTRAINT FK_67249AEA719FB48E FOREIGN KEY (contacts_id) REFERENCES contacts (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
	}

	public function down(Schema $schema): void
	{
		// this down() migration is auto-generated, please modify it to your needs
		$this->addSql('CREATE SCHEMA public');
		$this->addSql('ALTER TABLE worksite_contacts DROP CONSTRAINT FK_67249AEA719FB48E');
		$this->addSql('DROP SEQUENCE contacts_id_seq CASCADE');
		$this->addSql('DROP TABLE contacts');
		$this->addSql('DROP TABLE worksite_contacts');
	}
}
